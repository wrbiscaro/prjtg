<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.ProdutoDAO"%>
<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="br.edu.fatec.model.Produto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();
    ProdutoDAO produtoDao = new ProdutoDAO(conexao.conectar());
    ArrayList<Produto> produtos = produtoDao.listar();
    
    //Necessario adicionar os produtos no escopo de requisicao, para ser acessado por EL
    request.setAttribute("produtos", produtos);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>
            
        <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
            <br/><br/>
        </div>
        
        <div class="container-fluid">
            <!--Carousel-->
            <div id="myCarousel" class="carousel slide hidden-xs hidden-sm" data-ride="carousel" style="margin-top: 50px;"> 
                <!-- Indicadores -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="img/carousel1600x480.png" alt="Produto 1">
                        <div class="carousel-caption">
                            <h1>Produto 1</h1>
                            <p>Experimente o delicioso Produto 1!</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/carousel1600x480.png" alt="Produto 2">
                        <div class="carousel-caption">
                            <h1>Produto 2</h1>
                            <p>Novo Produto 2. Peça já!</p>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/carousel1600x480.png" alt="Produto 3">
                        <div class="carousel-caption">
                            <h3>Produto 3</h3>
                            <p>Vegetariano Produto 3. Apenas x calorias!</p>
                        </div>
                    </div>
                </div>

                <!-- Controladores esquerda e direita -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="row">              
                <div id="msg" style="text-align:center;color: red;">
                    <h5>${msg}</h5>
                </div> 
            
                <div class="col-md-2 hidden-sm hidden-xs">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                            <a href="#lanches">Lanches</a>
                        </li>
                        <li class="">
                            <a href="#pizzas">Pizzas</a>
                        </li>
                        <li class="">
                            <a href="#bebidas">Bebidas</a>
                        </li>
                        <li class="">
                            <a href="#sobremesas">Sobremesas</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <!-- Percorre produtos montando o cardapio -->
                        <c:forEach var="p" items="${produtos}">
                            <div class="col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <img src="http://localhost:8084/prjTG/Servlet?classe=Produto&cod=${p.cod}" class="img-thumbnail" width="300" height="300">
                                    <!--img-responsive faz a imagem ficar flexível e nunca estourar o tamanho do pai. img-thumbnail faz a imagem ficar centralizada com uma borda de destaque-->
                                    <!--É possivel esconder a imagem adicionando a classe hidden-xs do bootstrap, por exemplo, otimizando assim o layout p/ dispositivos pequenos-->
                                    <div class="caption center" style="text-align: center; height:170px;">
                                        <h3>${p.descricao}</h3>
                                        <p>
                                            <c:forEach var="i" items="${p.ingrediente}" varStatus="contador">                                        
                                                ${i.descricao}${!contador.last ? ',' : ''}
                                            </c:forEach>
                                        </p>
                                        <p>
                                            <strong>
                                                <fmt:formatNumber value="${p.preco}" type="currency" currencyCode="BRL"/>
                                            </strong>
                                        </p>
                                    </div>
                                    <p style="text-align: center;"> 
                                        <b>Quantidade</b> <input type="number" class="form-control text-center" id="qtde_${p.cod}" style="width: 80px;margin-left: 77px;" min="1" max="20" value="1">
                                    </p>
                                    <p style="text-align: center;">
                                    <a class="btn btn-primary" onclick="addCarrinho(${p.cod})">
                                        <span class="glyphicon glyphicon-shopping-cart"></span>
                                            Adicionar ao carrinho                                
                                        </a>
                                    </p>
                                </div>
                            </div>                               
                        </c:forEach>
                    </div>
                    
                    <div class="row text-center">
                        <ul class="pagination">
                            <li>
                                <a href="">«</a>
                            </li>
                            <li>
                                <a href="">1</a>
                            </li>
                            <!--
                            <li>
                                <a href="">2</a>
                            </li>
                            <li>
                                <a href="">3</a>
                            </li>
                            <li>
                                <a href="">4</a>
                            </li>
                            <li>
                                <a href="">5</a>
                            </li>
                            -->
                            <li>
                                <a href="">»</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        });       
    });
    
    function addCarrinho(codproduto)
    {
        var qtde = $("#qtde_"+codproduto).val();

        $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxCarrinho&codproduto=" + codproduto + "&qtde=" + qtde + "&acao=adicionar",
            success: function(msg){	
                $("#div_carrinho").html(msg);
                alert("Produto adicionado com sucesso!");
            }
        });	
    }
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
</script>