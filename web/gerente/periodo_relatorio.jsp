<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    String relatorio = request.getParameter("relatorio");
    request.setAttribute("relatorio", relatorio);
    
    Conexao conexao = new Conexao();
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <!--<link rel="stylesheet" href="../css/bootstrap-datepicker.css">-->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-lg-2">
                </div>
                <form class="col-sm-8 col-lg-8" action="../Servlet" method="POST">
                    <input type="hidden" name="classe" value="LogicaPedido" />
                    <input type="hidden" name="Pedido" value="GerarRelatorio" readonly="readonly" />
                    
                    <legend>Periodo do Relatório</legend>
                    <div class="row">
                        <input type="hidden" name="relatorio" value="${relatorio}"/>
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label>Data Inicial</label>
                                <input type="text" class="form-control" id="data_inicial" name="data_inicial">
                            </div>
                        </fieldset>
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label>Data Final</label>
                                <input type="text" class="form-control" id="data_final" name="data_final">
                            </div>                           
                        </fieldset>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg pull-right" id="gerar">
                        <span class="glyphicon glyphicon-ok"></span>
                        Gerar Relatório
                    </button>
                </form>
                <div class="col-sm-2 col-lg-2">
                </div>
            </div>                           
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="../js/jquery-2.1.3.js"></script>
        <script src="../js/bootstrap.js"></script>
        <!--<script src="../js/bootstrap-datepicker.js"></script>-->
        <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
        <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
        <script src="../js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){        
        $('#data_inicial').datepicker({
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
        });       
        
        $('#data_final').datepicker({
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
        });
        
        $("#data_inicial").mask("99/99/9999");
        $("#data_final").mask("99/99/9999");
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
</script>