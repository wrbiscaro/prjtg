<%@page import="br.edu.fatec.model.Endereco"%>
<%@page import="br.edu.fatec.controller.EnderecoDAO"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<%  
    Conexao conexao = new Conexao();
    EnderecoDAO enderecoDao = new EnderecoDAO(conexao.conectar());
    
    Usuario usuario = (Usuario)session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    ArrayList<Endereco> enderecos = enderecoDao.selecionarPorUsuario(usuario.getCod());
    
    //Necessario adicionar os enderecos no escopo de requisicao, para ser acessado por EL
    request.setAttribute("enderecos", enderecos);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/endereco.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h3 style="margin-bottom: 20px;">Selecione um endereço para alterar ou insira um novo endereço:</h3>
            <form action="fecharpedido.jsp" method="POST">
                <div class="row form-group product-chooser">
                    <c:forEach var="e" items="${enderecos}">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="product-chooser-item">
                                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                    <!--<span class="title">Endereco 1</span>-->
                                    <span class="description"><b>CEP:</b> ${e.cep}</span><br/>
                                    <span class="description"><b>Endereço:</b> ${e.endereco}</span></br>
                                    <span class="description"><b>Número:</b> ${e.numero}</span></br>
                                    <span class="description"><b>Complemento:</b> ${e.complemento}</span></br>
                                    <span class="description"><b>Bairro:</b> ${e.bairro}</span></br>
                                    <span class="description"><b>Cidade:</b> ${e.cidade}</span></br>
                                    <span class="description"><b>UF:</b> ${e.uf}</span></br>
                                    <input type="radio" id="radio_enderecos" name="radio_enderecos" value="${e.cod}">
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>                    
                    </c:forEach>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="product-chooser-item">
                                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                    <div style="width:270px;height:140px;text-align:center;">
                                        <img src="../img/add-address.png" width="150" height="150" align="center">
                                        <input type="radio" id="radio_enderecos" name="radio_enderecos" value="0">
                                    </div>                                   
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                </div>
                <!--<center>  
                    <button style="width: 150px;" class="btn btn-default btn-lg" onclick="voltar()">
                        Voltar
                    </button>
                </center>-->
            </form>
            <br/>
        </div>       
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="../js/jquery-2.1.3.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        }); 
        
        /*JS para selecao de endereco*/
        $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
            $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
            $(this).addClass('selected');
            $(this).find('input[type="radio"]').prop("checked", true);
            var codendereco =  $(this).find('input[type="radio"]').val();
            
            if(codendereco == 0)
                location.href = "cadastrar_endereco.jsp";
            else
                location.href = "alterar_endereco.jsp?codendereco=" + codendereco;
        });
    });
    
    function voltar(){
        history.back();
    }
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
</script>