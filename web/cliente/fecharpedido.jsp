<%@page import="br.edu.fatec.model.Pedido"%>
<%@page import="br.edu.fatec.model.Endereco"%>
<%@page import="br.edu.fatec.controller.EnderecoDAO"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();
    
    //Adicionado codendereco no escopo de requisicao, para ser acessado por EL
    request.setAttribute("codendereco", request.getParameter("radio_enderecos"));
    
    Pedido pedido = (Pedido)session.getAttribute("pedido");
    
    //Necessario adicionar os itenspedido no escopo de requisicao, para ser acessado por EL
    if(pedido != null)
    {
        request.setAttribute("itenspedido", pedido.getItempedido());
        request.setAttribute("numitens", pedido.getItempedido().size());
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h4 style="color: red;">${msg}</h4>
            <form action="../Servlet" method="POST">
                <input type="hidden" id="codendereco" name="codendereco" value="${codendereco}">
                <input type="hidden" name="classe" value="LogicaPedido" />
                <input type="hidden" name="Pedido" value="Inserir" readonly="readonly" />
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" align="center" style="width: 750px;">
                        <thead>
                            <tr style="text-align: center;">
                                <th>Produto</th>
                                <th>Quantidade</th>
                                <th>Valor Unitário</th>
                                <th>Valor Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="ip" items="${itenspedido}">
                                <tr style="text-align: center;">
                                    <td>${ip.produto.descricao}</td>
                                    <td>${ip.qtde}</td>
                                    <td>R$ ${ip.produto.preco}</td>
                                    <td>R$ ${ip.valor}</td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr style="text-align: center;">
                                <td colspan="2">&nbsp;</td>
                                <td>Taxa de Entrega</td>
                                <td>R$ 2.50</td>
                            </tr>
                            <tr class="success" style="text-align: center;font-size: 14px;">
                                <td colspan="2">&nbsp;</td>
                                <td><b>Total do pedido</b></td>
                                <td><b>R$ ${pedido.valor_total + 2.50}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-2 hidden-xs"></div>
                    <div class="col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                Forma de Pagamento
                                <select class="form-control" id="forma_pagamento" name="forma_pagamento">
                                    <option value="1">Dinheiro</option>
                                    <option value="2">Cartão</option>
                                </select>
                            </div>
                            <div id="div_troco" class="col-sm-6 col-xs-12">
                                Troco para
                                <input class="form-control" type="text" id="troco" name="troco">
                            </div>
                        </div>
                        <br/>
                        Observações
                        <textarea class="form-control" id="observacoes" name="observacoes" placeholder="Digite aqui as observações do seu pedido (Ex.: Lanche sem tomate)."></textarea>
                        <br/>
                        <center>  
                            <button style="width: 150px;" class="btn btn-default btn-lg" onclick="voltar()">
                                Alterar Pedido
                            </button>
                            <button style="width: 150px;" type="submit" class="btn btn-success btn-lg">
                                Finalizar Pedido
                            </button>
                        </center>
                    </div>
                    <div class="col-sm-2 hidden-xs"></div>
                </div>
            </form>
        </div>        
        <br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="../js/jquery-2.1.3.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.maskedinput.js"></script>
        <script src="../js/jquery.maskMoney.min.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        }); 
        
        $("#troco").maskMoney();
    });
    
    function voltar(){
        location.href("../carrinho.jsp");
    }
    
    $("#forma_pagamento").change(function(){
       var opcao =  $("#forma_pagamento").val();
       
       if(opcao == 2)
           $("#div_troco").hide();
       else
           $("#div_troco").show();
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
</script>