<%@page import="br.edu.fatec.model.Endereco"%>
<%@page import="br.edu.fatec.controller.EnderecoDAO"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();
    EnderecoDAO enderecoDao = new EnderecoDAO(conexao.conectar());
    
    int codendereco = Integer.parseInt(request.getParameter("codendereco"));

    Endereco endereco = enderecoDao.selecionar(codendereco);
    request.setAttribute("endereco", endereco);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="http://localhost:8084/prjTG/css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <div class="row">
                <!--<div class="col-sm-8 col-lg-9">
                </div>
                <div class="col-sm-4 col-lg-3">
                </div>-->
                <div class="col-sm-2 col-lg-2">
                </div>
                <form class="col-sm-8 col-lg-8" action="http://localhost:8084/prjTG/Servlet" method="POST">
                    <input type="hidden" name="classe" value="LogicaEndereco" />
                    <input type="hidden" name="Endereco" value="Alterar" readonly="readonly" /> 
                    <input type="hidden" id="codendereco" name="codendereco" value="${endereco.cod}" readonly="readonly" />               

                    <div id="msg" style="text-align:center;color: red;">
                        <h4>${msg}</h4>
                    </div> 
                    
                    <legend>Dados do endereço</legend>
                    <div class="row">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="cep">CEP</label>
                                <input type="text" class="form-control" id="cep" name="cep" value="${endereco.cep}" autofocus required>
                            </div>
                            
                            <div class="form-group">
                                <label for="complemento">Complemento</label>
                                <input type="text" class="form-control" id="complemento" name="complemento" value="${endereco.complemento}">
                            </div>
                            
                            <div class="form-group">
                                <label for="bairro">Bairro</label>
                                <input type="text" class="form-control" id="bairro" name="bairro" value="${endereco.bairro}" readonly required>
                            </div>
                            
                            <div class="form-group">
                                <label for="estado">Estado</label>
                                <input type="text" class="form-control" id="estado" name="estado" value="${endereco.uf}" readonly required>
                            </div>    
                        </fieldset>
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="numero">Numero</label>
                                <input type="text" class="form-control" id="numero" name="numero" value="${endereco.numero}" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="endereco">Endereço</label>
                                <input type="text" class="form-control" id="endereco" name="endereco" value="${endereco.endereco}" readonly required>
                            </div>
                            
                            <div class="form-group">
                                <label for="cidade">Cidade</label>
                                <input type="text" class="form-control" id="cidade" name="cidade" value="${endereco.cidade}" readonly required>
                            </div>       
                        </fieldset>
                    </div>
                    <button type="button" class="btn btn-primary btn-lg pull-left" onclick="correios();">
                        <span class="glyphicon glyphicon-search"></span>
                        Pesquisar CEP
                    </button>
                    <button type="submit" class="btn btn-primary btn-lg pull-right" id="alterar">
                        <span class="glyphicon glyphicon-ok"></span>
                         Alterar
                    </button>
                </form>
                <div class="col-sm-2 col-lg-2">
                </div>
            </div>
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="http://localhost:8084/prjTG/js/jquery-2.1.3.js"></script>
        <script src="http://localhost:8084/prjTG/js/bootstrap.js"></script>
        <script src="http://localhost:8084/prjTG/js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "http://localhost:8084/prjTG/Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        });  
        
        $("#cep").mask("99999-999");
        $("#dtnasc").mask("99/99/9999");
        $("#telefone").mask("(99) 99999-9999");
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "http://localhost:8084/prjTG/Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    
    function correios()
    {
        var cep = $("#cep").val();					
        cepArray = cep.split("-");
        cep = cepArray[0]+cepArray[1];
									
	if(cep != "")
	{									
            $.ajax({
                type: "POST",
                url: "http://localhost:8084/prjTG/Servlet",
                data: "classe=AjaxEndereco&acao=consultar&cep=" + cep,
                success: function(msg){	
                    if(msg.trim() == "Invalido")
                    {                        
                        $("#endereco").val("");
                        $("#bairro").val("");
                        $("#cidade").val("");
                        $("#estado").val("");

                        $("#alterar").attr('disabled', 'disabled'); 
                        
                        alert("Endereço não atendido pelas nossas unidades!");
                    }
                    else{
                        var endereco = msg.split(";");

                        $("#endereco").val(endereco[1]);
                        $("#bairro").val(endereco[2]);
                        $("#cidade").val(endereco[3]);
                        $("#estado").val(endereco[4]);

                        $("#alterar").removeAttr('disabled');                        
                    }										
                }
            });	
        }
    }
</script>