<%-- 
    Document   : teste_email
    Created on : 01/05/2015, 03:22:59
    Author     : luizdagoberto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Teste de Envio de Email</title>
    </head>
    <body>
        <h2>Teste de Envio de Email</h2>
        
        <form method="POST" action="Servlet">
            <input type="hidden" name="classe" value="LogicaEmail" />
            <input type="hidden" name="Email" value="Enviar" readonly="readonly" />
            
            <div style="color: red;" id="resultado" name="resultado">${msg}</div>
            
            <input type="submit" value="Enviar Email">
        </form>
    </body>
</html>
