<%@page import="javax.swing.ImageIcon"%>
<%@page import="br.edu.fatec.model.ItemProduto"%>
<%@page import="br.edu.fatec.controller.ItemProdutoDAO"%>
<%@page import="br.edu.fatec.model.Produto"%>
<%@page import="br.edu.fatec.controller.ProdutoDAO"%>
<%@page import="br.edu.fatec.controller.IngredienteDAO"%>
<%@page import="br.edu.fatec.model.Ingrediente"%>
<%@page import="br.edu.fatec.model.Funcionario"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();
    ProdutoDAO produtoDao = new ProdutoDAO(conexao.conectar());
    ItemProdutoDAO itemProdutoDao = new ItemProdutoDAO(conexao.conectar());

    int codproduto = request.getParameter("codproduto") == null ? 0 : Integer.parseInt(request.getParameter("codproduto"));
    
    if(codproduto != 0){
        Produto p = produtoDao.selecionar(codproduto);
        
        request.setAttribute("produto", p);
        
        ArrayList<ItemProduto> itemproduto = itemProdutoDao.listarPorProduto(codproduto);
        request.setAttribute("produto", p);
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="http://localhost:8084/prjTG/css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
            .searchable-container{margin:20px 0 0 0}
            .searchable-container label.btn-default.active{background-color:#007ba7;color:#FFF}
            .searchable-container label.btn-default{width:90%;border:1px solid #efefef;margin:5px; box-shadow:5px 8px 8px 0 #ccc;}
            .searchable-container label .bizcontent{width:100%;}
            .searchable-container .btn-group{width:90%}
            .searchable-container .btn span.glyphicon{
                opacity: 0;
            }
            .searchable-container .btn.active span.glyphicon {
                opacity: 1;
            }
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <form action="http://localhost:8084/prjTG/Servlet" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="classe" value="LogicaProduto" />
                <input type="hidden" name="Produto" value="Alterar" readonly="readonly" /> 
                <input type="hidden" id="codproduto" name="codproduto" value="${produto.cod}" readonly="readonly" />
 
                <div id="msg" style="text-align:center;color: red;">
                    <h4>${msg}</h4>
                </div>
                
                <div class="row">
                    <h3 style="text-align:center;">Alteração de Produto</h3>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail">
                        <img src="http://localhost:8084/prjTG/Servlet?classe=Produto&cod=${produto.cod}" class="img-thumbnail" width="300" height="300">
                        <!--img-responsive faz a imagem ficar flexível e nunca estourar o tamanho do pai. img-thumbnail faz a imagem ficar centralizada com uma borda de destaque-->
                        <!--É possivel esconder a imagem adicionando a classe hidden-xs do bootstrap, por exemplo, otimizando assim o layout p/ dispositivos pequenos-->
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="descricao">Descrição</label>
                            <input type="text" class="form-control" id="descricao" name="descricao" value="${produto.descricao}" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="unidade">Unidade</label>
                            <select class="form-control" id="unidade" name="unidade">
                                <option value="Un" ${produto.unidade == 'Un' ? 'selected="selected"' : ''}>Un.</option>
                                <option value="Kg" ${produto.unidade == 'Kg' ? 'selected="selected"' : ''}>Kg.</option>
                                <option value="Gr" ${produto.unidade == 'Gr' ? 'selected="selected"' : ''}>Gr.</option>
                                <option value="Lt" ${produto.unidade == 'Lt' ? 'selected="selected"' : ''}>Lt.</option>
                                <option value="Ml" ${produto.unidade == 'Ml' ? 'selected="selected"' : ''}>Ml.</option>
                            </select>
                        </div> 
                        
                        <div class="form-group">
                            <label for="foto">Foto</label>
                            <input type="file" class="btn btn-default" id="foto" name="foto">
                        </div> 
                        
                        <!-- Retirada a opcao de alterar ingredientes do produto
                        <div class="form-group">
                            <label for="ingrediente">
                                <input type="checkbox" class="btn btn-default" id="ingrediente" name="ingrediente" onclick="mostrarIngredientes(this)">
                                Adicionar ingredientes ao produto
                            </label>
                        </div>-->                         
                    </div>
                    
                    <div class="col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="preco">Preço</label>
                            <input type="text" class="form-control" id="preco" name="preco" value="${produto.preco}" required>
                        </div>

                        <div class="form-group">
                            <label for="categoria">Categoria</label>
                            <select class="form-control" id="categoria" name="categoria">
                                <option value="L" ${produto.categoria == 'L' ? 'selected="selected"' : ''}>Lanches</option>
                                <option value="P" ${produto.categoria == 'P' ? 'selected="selected"' : ''}>Pizzas</option>
                                <option value="B" ${produto.categoria == 'B' ? 'selected="selected"' : ''}>Bebidas</option>
                                <option value="S" ${produto.categoria == 'S' ? 'selected="selected"' : ''}>Sobremesas</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row" id="div_ingredientes" name="div_ingredientes" style="display: none;">
                    <div class="row">
                        <h3 style="text-align:center;">Adicionar Ingredientes</h3>
                        <div class="form-group">
                            <center>
                                <div class="col-md-1 col-lg-2 hidden-sm"></div>
                                <div class="col-sm-12 col-md-10 col-lg-8">
                                    <input type="search" class="form-control" id="search" placeholder="Pesquise um ingrediente...">
                                </div>
                                <div class="col-md-1 col-lg-2 hidden-sm"></div>
                            </center>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="searchable-container">
                            <c:forEach var="i" items="${ingredientes}">
                                <div class="items col-xs-5 col-sm-5 col-md-3 col-lg-3">
                                    <div class="info-block block-info clearfix">
                                        <div class="square-box pull-left">
                                            <span class="glyphicon glyphicon-tags glyphicon-lg"></span>
                                        </div>
                                        <div data-toggle="buttons" class="btn-group bizmoduleselect">
                                            <label class="btn btn-default">
                                                <div class="bizcontent">
                                                    <input type="checkbox" name="ingrediente" autocomplete="off" value="${i.cod}">
                                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>
                                                    <h5>${i.descricao}</h5>
                                                </div>
                                            </label>                           
                                        </div>
                                    </div>
                                    <br/>
                                    <input class="form-control" type="text" placeholder="Quantidade Padrão: ${i.qtde_padrao}" id="qtd_${i.cod}" name="qtd_${i.cod}">
                                    <br/>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-lg pull-right" id="alterar">
                    <span class="glyphicon glyphicon-ok"></span>
                     Alterar
                </button>
            </form>
        </div>
        <br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="http://localhost:8084/prjTG/js/jquery-2.1.3.js"></script>
        <script src="http://localhost:8084/prjTG/js/bootstrap.js"></script>
        <script src="http://localhost:8084/prjTG/js/jquery.maskedinput.js"></script>
        <script src="http://localhost:8084/prjTG/js/jquery.maskMoney.min.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){ 
        if($("#estoque").is(":checked"))
            $("#div_qtde_minima").show();
        else
        {
            $("#qtde_minima").val("");
            $("#div_qtde_minima").hide();
        }
        
        $('#search').on('keyup', function() {
            var pattern = $(this).val();
            $('.searchable-container .items').hide();
            $('.searchable-container .items').filter(function() {
                return $(this).text().match(new RegExp(pattern, 'i'));
            }).show();
        });
        
        $("#preco").maskMoney();
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "http://localhost:8084/prjTG/Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    
    function Estoque()
    {
        if($("#estoque").is(":checked"))
            $("#div_qtde_minima").show();
        else
        {
            $("#qtde_minima").val("");
            $("#div_qtde_minima").hide();
        }
    }
    
    function mostrarIngredientes(chkingrediente)
    {
        if($(chkingrediente).is(":checked"))
            $("#div_ingredientes").show();
        else
            $("#div_ingredientes").hide();
    }
</script>