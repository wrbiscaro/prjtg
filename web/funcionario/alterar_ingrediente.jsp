<%@page import="br.edu.fatec.controller.IngredienteDAO"%>
<%@page import="br.edu.fatec.model.Ingrediente"%>
<%@page import="br.edu.fatec.model.Familia"%>
<%@page import="br.edu.fatec.controller.FamiliaDAO"%>
<%@page import="br.edu.fatec.model.Estoque"%>
<%@page import="br.edu.fatec.controller.EstoqueDAO"%>
<%@page import="br.edu.fatec.model.Funcionario"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();
    IngredienteDAO ingredienteDao = new IngredienteDAO(conexao.conectar());
    EstoqueDAO estoqueDao = new EstoqueDAO(conexao.conectar());
    FamiliaDAO familiaDao = new FamiliaDAO(conexao.conectar());
    ArrayList<Familia> familias = familiaDao.listar();
    
    int codingrediente = request.getParameter("codingrediente") == null ? 0 : Integer.parseInt(request.getParameter("codingrediente"));
    
    if(codingrediente != 0){
        Ingrediente i = ingredienteDao.selecionar(codingrediente);
        request.setAttribute("ingrediente", i);
        
        if(i.getEstoque().equals("S")) {
            Estoque e = estoqueDao.selecionar(1, codingrediente);
            double estoque_minimo = e.getEstoque_minimo();
            request.setAttribute("estoque_minimo", estoque_minimo);
        }

    }
    
    //Add no escopo de requisicao para ser acessado por EL
    request.setAttribute("familias", familias);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="http://localhost:8084/prjTG/css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
            div.display{display:inline-table;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h3 style="text-align:center;">Alteração de Ingrediente</h3>
            <div id="div_msg" style="text-align:center;color: red;">
                <h4>${msg}</h4>
            </div> 
          
            <div class="row">
                <div class="col-sm-2 col-lg-2">
                </div>
                <form class="col-sm-8 col-lg-8" action="http://localhost:8084/prjTG/Servlet" method="POST">
                    <input type="hidden" name="classe" value="LogicaIngrediente" />
                    <input type="hidden" name="Ingrediente" value="Alterar" readonly="readonly" />
                    <input type="hidden" name="codingrediente" value="${ingrediente.cod}" readonly="readonly" />
                    
                    <div class="row">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="descricao">Descrição</label>
                                <input type="text" class="form-control" id="descricao" name="descricao" value="${ingrediente.descricao}" autofocus required>
                            </div>
                            
                            <div class="form-group">
                                <label for="familia">Familia</label>
                                <select class="form-control" id="familia" name="familia">
                                    <c:forEach var="f" items="${familias}">
                                        <option value="${f.cod}" ${f.cod == ingrediente.familia.cod ? 'selected="selected"' : ''}>${f.descricao}</option>
                                    </c:forEach>
                                </select>
                            </div> 
                            
                            <div class="checkbox">
                                <br/>
                                <label>
                                    <input type="checkbox" id="estoque" name="estoque" onclick="Estoque();" ${ingrediente.estoque == "S" ? 'checked' : ''}> Armazenar ingrediente no estoque
                                </label>
                            </div>  
                        </fieldset>
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="unidade">Unidade</label>
                                <select class="form-control" id="unidade" name="unidade">
                                    <option value="Un" ${ingrediente.unidade == "Un" ? 'selected="selected"' : ''}>Un.</option>
                                    <option value="Kg" ${ingrediente.unidade == "Kg" ? 'selected="selected"' : ''}>Kg.</option>
                                    <option value="Gr" ${ingrediente.unidade == "Gr" ? 'selected="selected"' : ''}>Gr.</option>
                                    <option value="Lt" ${ingrediente.unidade == "Lt" ? 'selected="selected"' : ''}>Lt.</option>
                                    <option value="Ml" ${ingrediente.unidade == "Ml" ? 'selected="selected"' : ''}>Ml.</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="qtde_padrao">Quantidade Padrão</label>
                                <input type="text" class="form-control" id="qtde_padrao" name="qtde_padrao" value="${ingrediente.qtde_padrao}">
                            </div>
                            
                            <div class="form-group" id="div_qtde_minima" style="display: none;">
                                <label for="qtde_minima">Estoque Mínimo</label>
                                <input type="text" class="form-control" id="qtde_minima" name="qtde_minima" value="${estoque_minimo}">
                            </div>                            
                        </fieldset>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg pull-right" id="cadastrar">
                        <span class="glyphicon glyphicon-ok"></span>
                        Alterar
                    </button>
                </form>
                <div class="col-sm-2 col-lg-2">
                </div>
            </div>
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="http://localhost:8084/prjTG/js/jquery-2.1.3.js"></script>
        <script src="http://localhost:8084/prjTG/js/bootstrap.js"></script>
        <script src="http://localhost:8084/prjTG/js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){ 
        if($("#estoque").is(":checked"))
            $("#div_qtde_minima").show();
        else
        {
            $("#qtde_minima").val("");
            $("#div_qtde_minima").hide();
        }
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "http://localhost:8084/prjTG/Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    
    function Estoque()
    {
        if($("#estoque").is(":checked"))
            $("#div_qtde_minima").show();
        else
        {
            $("#qtde_minima").val("");
            $("#div_qtde_minima").hide();
        }
    }
</script>