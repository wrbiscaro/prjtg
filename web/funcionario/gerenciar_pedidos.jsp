<%@page import="br.edu.fatec.model.Funcionario"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@page import="br.edu.fatec.model.Pedido"%>
<%@page import="br.edu.fatec.controller.PedidoDAO"%>
<%@page import="br.edu.fatec.model.Cliente"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Funcionario f = (Funcionario)session.getAttribute("funcionario");
    
    Conexao conexao = new Conexao();
    PedidoDAO pedidoDao = new PedidoDAO(conexao.conectar());
    ArrayList<Pedido> pedidos = pedidoDao.listarPorLoja(f.getLoja().getCod());
    
    //Add no escopo de requisicao para ser acessado por EL
    request.setAttribute("pedidos", pedidos);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
            
            .clickable
            {
                cursor: pointer;
            }

            .clickable .glyphicon
            {
                background: rgba(0, 0, 0, 0.15);
                display: inline-block;
                padding: 6px 12px;
                border-radius: 4px
            }

            .panel-heading span
            {
                margin-top: -23px;
                font-size: 15px;
                margin-right: -9px;
            }
            a.clickable { color: inherit; }
            a.clickable:hover { text-decoration:none; }
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h3 style="text-align:center;">Gerenciamento de Pedidos</h3>
            <div>
                <label>
                    Data do Pedido: 
                    <input class="" type="text" id="data" name="date" value="24/05/2015">
                    <span class="glyphicon glyphicon-search"></span>                    
                </label>
            </div>
            <div id="div_msg" style="text-align:center;color: red;">
                <h4>${msg}</h4>
            </div> 
            
            <c:forEach var="p" items="${pedidos}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <b>Número do Pedido:</b> #${p.cod} &emsp;
                                    <b>Data:</b> ${p.data_hora} &emsp;
                                    <b>Valor do Pedido:</b> ${p.valor_total + p.taxa_entrega} &emsp;
                                    <b>Status: </b>
                                    <select class="btn btn-danger" id="select_${p.cod}" name="select_${p.cod}">
                                        <option value="1" ${p.status == 1 ? "selected=" : ""}>Aberto</option>
                                        <option value="2" ${p.status == 2 ? "selected=" : ""}>Aceito</option>
                                        <option value="3" ${p.status == 3 ? "selected=" : ""}>Em Preparação</option>
                                        <option value="4" ${p.status == 4 ? "selected=" : ""}>Pronto</option>
                                        <option value="5" ${p.status == 5 ? "selected=" : ""}>Saiu para entrega</option>
                                        <option value="6" ${p.status == 6 ? "selected=" : ""}>Recusado</option>
                                        <option value="7" ${p.status == 7 ? "selected=" : ""}>Entregue</option>
                                        <option value="8" ${p.status == 8 ? "selected=" : ""}>Não Entregue</option>
                                    </select>
                                    &emsp;&emsp;
                                    <input type="button" class="btn btn-danger" id="atualizar" name="atualizar" onclick="atualizarStatus(${p.cod})" value="Atualizar">
                                </h3>
                                <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" align="center" style="width: 750px;">
                                        <thead>
                                            <tr style="text-align: center;">
                                                <th>Produto</th>
                                                <th>Quantidade</th>
                                                <th>Valor Unitário</th>
                                                <th>Valor Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="ip" items="${p.itempedido}">
                                                <tr style="text-align: center;">
                                                    <td>${ip.produto.descricao}</td>
                                                    <td>${ip.qtde}</td>
                                                    <td>R$ ${ip.produto.preco}</td>
                                                    <td>R$ ${ip.valor}</td>
                                                </tr>
                                            </c:forEach>
                                            <tr>
                                                <td colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <td colspan="2">&nbsp;</td>
                                                <td>Taxa de Entrega</td>
                                                <td>R$ 2.50</td>
                                            </tr>
                                            <tr class="info" style="text-align: center;font-size: 14px;">
                                                <td colspan="2">&nbsp;</td>
                                                <td><b>Total do pedido</b></td>
                                                <td><b>R$ ${p.valor_total + 2.50}</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>               
            </c:forEach>
        </div>
        <br/><br/>
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="../js/jquery-2.1.3.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){ 
        $('.panel-heading span.clickable').click();
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    
    function atualizarStatus(codpedido)
    {
        $("#div_msg").html("");
        
        var status = $("#select_" + codpedido).val();
      
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxPedido&acao=atualizarStatus&codpedido=" + codpedido + "&status=" + status,
            success: function(msg){
                //location.reload();
                $("#div_msg").html(msg);  
            }
        });
        
        $('html, body').animate({scrollTop: 0}, 100, 'linear'); //Faz a pagina ir para o topo
    }
    
    $(document).on('click', '.panel-heading span.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }
    });
</script>