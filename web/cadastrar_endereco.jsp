<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod()); 
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-lg-2"></div>
                <form class="col-sm-8 col-lg-8" method="POST" action="Servlet">
                    <input type="hidden" name="classe" value="LogicaEndereco" />
                    <input type="hidden" name="Endereco" value="Consultar" readonly="readonly" />
                    
                    <h5 style="text-align: center;">Caro cliente, antes de prosseguir, é necessário verificar se seu endereço é atendido por uma de nossas unidades.</h5>
                    <br/>
                    <legend>Informe seu endereço</legend>
                    <div class="row">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="cep">CEP</label>
                                <input id="cep" class="form-control" type="text" required="required" autofocus="" name="cep">
                            </div>
                        </fieldset>
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="numero">Numero</label>
                                <input id="numero" class="form-control" type="text" required="required" name="numero">
                            </div>
                        </fieldset>
                    </div>
                    <center>
                        <button id="calcular" class="btn btn-primary btn-lg" type="submit">
                            <span class="glyphicon glyphicon-search"></span> 
                            Consultar
                        </button>
                        <br/>
                        <small>                            
                            <a href="http://www.buscacep.correios.com.br/servicos/dnec/index.do" target="_blank">
                                Não sabe seu CEP? Clique aqui para consultar.   
                            </a>
                        </small>
                        <br/>
                        <div style="color: red">
                            <h5><b>${msg}</b></h5>
                        </div>
                    </center>
                </form>
                <div class="col-sm-2 col-lg-2"></div>
            </div>
        </div>
        <br/><br/>
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        });  
        
        $("#cep").mask("99999-999");
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
</script>