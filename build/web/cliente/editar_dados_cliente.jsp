<%@page import="br.edu.fatec.controller.UsuarioDAO"%>
<%@page import="br.edu.fatec.controller.ClienteDAO"%>
<%@page import="br.edu.fatec.model.Cliente"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();
    
    String codusuario = request.getParameter("codusuario");

    if(codusuario != null){ //Teve requisicao por get, portanto é o gerente que esta alterando o cliente
        UsuarioDAO usuarioDao = new UsuarioDAO(conexao.conectar());
        usuario = usuarioDao.selecionar(Integer.parseInt(codusuario));
        
        ClienteDAO clienteDao = new ClienteDAO(conexao.conectar());
        Cliente cliente = clienteDao.selecionarPorUsuario(Integer.parseInt(codusuario));
        
        request.setAttribute("cliente", cliente);
        request.setAttribute("usuario", usuario);
    }  
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="http://localhost:8084/prjTG/css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-lg-2">
                </div>
                <form class="col-sm-8 col-lg-8" action="http://localhost:8084/prjTG/Servlet" method="POST" onsubmit="return validaForm();">
                    <input type="hidden" name="classe" value="LogicaCliente" />
                    <input type="hidden" name="Cliente" value="Alterar" readonly="readonly" />   
                    <input type="hidden" id="codcliente" name="codcliente" value="${cliente.cod}" readonly="readonly" /> 
                    <input type="hidden" id="codusuario" name="codusuario" value="${usuario.cod}" readonly="readonly" /> 
                    <input type="hidden" id="senhaarmazenada" name="senhaarmazenada" value="${usuario.senha}" readonly="readonly" /> 

                    <div id="msg" style="text-align:center;color: red;">
                        <h4>${msg}</h4>
                    </div> 
            
                    <legend>Dados pessoais</legend>
                    <div class="row">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" id="nome" name="nome" value="${cliente.nome}" required>
                            </div>

                            <div class="form-group">
                                <label for="telefone">Telefone</label>
                                <input type="text" class="form-control" id="telefone" value="${cliente.telefone}" name="telefone" required>
                            </div>
                        </fieldset>
                        
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="dtnasc">Data de Nascimento</label>
                                <input type="text" class="form-control" id="dtnasc" name="dtnasc" value="${cliente.dataNasc}" required>
                            </div>
                        </fieldset>
                    </div>
                    
                    <legend>Dados de acesso</legend>
                    <div class="row">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="text" class="form-control" id="email" name="email" value="${usuario.email}" required>
                            </div>  
                            
                            <div class="form-group">
                                <label for="novasenha">Nova senha</label>
                                <input type="password" class="form-control" id="novasenha" name="novasenha">
                            </div>
                        </fieldset>
                        
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="senha">Senha Atual</label>
                                <input type="password" class="form-control" id="senha" name="senha">
                            </div>
                            
                            <div class="form-group">
                                <label for="repeticaosenha">Repita a nova senha</label>
                                <input type="password" class="form-control" id="repeticaosenha" name="repeticaosenha">
                            </div>
                        </fieldset>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg pull-right" id="calcular">
                        <span class="glyphicon glyphicon-ok"></span>
                        Alterar
                    </button>
                </form>
                <div class="col-sm-2 col-lg-2">
                </div>
            </div>                           
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="http://localhost:8084/prjTG/js/jquery-2.1.3.js"></script>
        <script src="http://localhost:8084/prjTG/js/bootstrap.js"></script>
        <script src="http://localhost:8084/prjTG/js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "http://localhost:8084/prjTG/Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        });  
        
        $("#cep").mask("99999-999");
        $("#dtnasc").mask("99/99/9999");
        $("#telefone").mask("(99) 99999-9999");
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "http://localhost:8084/prjTG/Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    
    function validaForm(){
        if($("#senha").val() != ""){
            if($("#senha").val() == $("#senhaarmazenada").val()){
                if($("#novasenha").val() == "" || $("#repeticaosenha").val() == ""){
                    alert("Nova senha ou repetição não informada!");
                    return false;                    
                }
                else if($("#novasenha").val() != $("#repeticaosenha").val()){
                    alert("Nova senha diferente da repetição!");
                    return false;
                }
                else
                    return true;
            }     
            else{
                alert("Senha atual incorreta!");
                return false;
            }
        }
        else
            return true;
    }
</script>