<%@page import="br.edu.fatec.model.Usuario"%>
<%@page import="br.edu.fatec.model.Pedido"%>
<%@page import="br.edu.fatec.controller.PedidoDAO"%>
<%@page import="br.edu.fatec.model.Cliente"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Cliente c = (Cliente)session.getAttribute("cliente");
    
    Conexao conexao = new Conexao();
    PedidoDAO pedidoDao = new PedidoDAO(conexao.conectar());
    ArrayList<Pedido> pedidos = pedidoDao.listarPorCliente(c.getCod());
    
    String msg = request.getParameter("msg");
    if((msg != null) && msg.equals("sucesso"))
        request.setAttribute("msg", "Pedido realizado com sucesso! Acompanhe seus pedidos abaixo.");
    
    //Add no escopo de requisicao para ser acessado por EL
    request.setAttribute("pedidos", pedidos);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
            
            .clickable
            {
                cursor: pointer;
            }

            .clickable .glyphicon
            {
                background: rgba(0, 0, 0, 0.15);
                display: inline-block;
                padding: 6px 12px;
                border-radius: 4px
            }

            .panel-heading span
            {
                margin-top: -23px;
                font-size: 15px;
                margin-right: -9px;
            }
            a.clickable { color: inherit; }
            a.clickable:hover { text-decoration:none; }
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h3>Meus Pedidos</h3>
            <div id="msg" style="text-align:center;color: red;">
                <h4>${msg}</h4>
            </div> 
            
            <c:forEach var="p" items="${pedidos}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <b>Número do Pedido:</b> #${p.cod} &emsp;
                                    <b>Data:</b> ${p.data_hora} &emsp;
                                    
                                    <c:if test="${p.status == 1}">
                                        <b>Status:</b> Aberto &emsp;
                                    </c:if>
                                    <c:if test="${p.status == 2}">
                                        <b>Status:</b> Aceito &emsp;
                                    </c:if>
                                    <c:if test="${p.status == 3}">
                                        <b>Status:</b> Em Preparação &emsp;
                                    </c:if>
                                    <c:if test="${p.status == 4}">
                                        <b>Status:</b> Pronto &emsp;
                                    </c:if>
                                    <c:if test="${p.status == 5}">
                                        <b>Status:</b> Saiu para entrega &emsp;
                                    </c:if>
                                    <c:if test="${p.status == 6}">
                                        <b>Status:</b> Recusado &emsp;
                                    </c:if>
                                    <c:if test="${p.status == 7}">
                                        <b>Status:</b> Entregue &emsp;
                                    </c:if>
                                    <c:if test="${p.status == 8}">
                                        <b>Status:</b> Não Entregue &emsp;
                                    </c:if>
                                        
                                    <b>Valor:</b> R$ ${p.valor_total} &emsp;
                                    <b>Taxa de Entrega:</b> R$ ${p.taxa_entrega}
                                </h3>
                                <span class="pull-right clickable"><i class="glyphicon glyphicon-minus"></i></span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped" align="center" style="width: 750px;">
                                        <thead>
                                            <tr style="text-align: center;">
                                                <th>Produto</th>
                                                <th>Quantidade</th>
                                                <th>Valor Unitário</th>
                                                <th>Valor Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="ip" items="${p.itempedido}">
                                                <tr style="text-align: center;">
                                                    <td>${ip.produto.descricao}</td>
                                                    <td>${ip.qtde}</td>
                                                    <td>R$ ${ip.produto.preco}</td>
                                                    <td>R$ ${ip.valor}</td>
                                                </tr>
                                            </c:forEach>
                                            <tr>
                                                <td colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <td colspan="2">&nbsp;</td>
                                                <td>Taxa de Entrega</td>
                                                <td>R$ 2.50</td>
                                            </tr>
                                            <tr class="info" style="text-align: center;font-size: 14px;">
                                                <td colspan="2">&nbsp;</td>
                                                <td><b>Total do pedido</b></td>
                                                <td><b>R$ ${p.valor_total + 2.50}</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>               
            </c:forEach>
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="../js/jquery-2.1.3.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        }); 
        
        $('.panel-heading span.clickable').click();
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    
    $(document).on('click', '.panel-heading span.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }
    });
</script>