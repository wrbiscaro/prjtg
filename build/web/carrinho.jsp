<%@page import="br.edu.fatec.model.Usuario"%>
<%@page import="br.edu.fatec.model.Pedido"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.ProdutoDAO"%>
<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="br.edu.fatec.model.Produto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();

    Pedido pedido = (Pedido)session.getAttribute("pedido");
    
    String msg = request.getParameter("msg");
    if((msg != null) && msg.equals("erro"))
        request.setAttribute("msg", "Pedido não finalizado! Entre em contato para maiores informações.");
    
    //Necessario adicionar os itenspedido no escopo de requisicao, para ser acessado por EL
    if(pedido != null)
    {
        request.setAttribute("itenspedido", pedido.getItempedido());
        request.setAttribute("numitens", pedido.getItempedido().size());
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <div id="msg" style="text-align:center;color: red;">
                <h3>${msg}</h3>
            </div> 
            
            <div class="row">
                <form method="POST" action="cliente/endereco.jsp">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <div class="row">
                                        <div class="col-md-6 hidden-sm hidden-xs">
                                            <h3 style="text-align:right;"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp; Carrinho</h3>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <a class="btn btn-primary btn-lg pull-right" href="cardapio.jsp">
                                                <span class="glyphicon glyphicon-share-alt"></span>&nbsp; Continue comprando
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <c:if test="${pedido == null || numitens == 0}">
                                    <h3 class="text-center">Não há produtos no carrinho!</h3>
                                </c:if>
                                <c:if test="${pedido != null && numitens > 0}">   
                                    <c:forEach var="ip" items="${itenspedido}">
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-2 hidden-xs">
                                                <img class="img-responsive" src="http://localhost:8084/prjTG/Servlet?classe=Produto&cod=${ip.produto.cod}">
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <h3 class="product-name"><strong>${ip.produto.descricao}</strong></h3>
                                                <h3>
                                                    <small>
                                                        <c:forEach var="i" items="${ip.produto.ingrediente}" varStatus="contador">
                                                            ${i.descricao}${!contador.last ? ',' : ''}
                                                        </c:forEach>
                                                    </small>
                                                </h3>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-6 col-xs-6 text-right">
                                                    <h4><strong>R$ ${ip.produto.preco}<span class="text-muted">&nbsp; x</span></strong></h4>
                                                </div>
                                                <div class="col-sm-4 col-xs-6">
                                                    <input type="number" id="qtde_${ip.produto.cod}" class="form-control input-lg text-center" max="20" min="1" value="${ip.qtde}" onclick="atualizarQtde(${ip.produto.cod})">
                                                </div>
                                                <div class="col-sm-2 col-xs-12">
                                                    <button type="button" class="btn btn-link btn-lg" onclick="removerProduto(${ip.produto.cod})">
                                                        <span class="glyphicon glyphicon-trash"> </span>
                                                    </button>
                                                </div>
                                            </div>                                    
                                        </div>
                                    </c:forEach>
                                </c:if>
                                <!--<hr>
                                <div class="row">
                                    <div class="text-center">
                                        <div class="col-sm-9 col-xs-12">
                                            <h3 class="text-right">Adicionou itens?</h3>
                                        </div>
                                        <div class="col-sm-3 col-xs-12">
                                            <button type="button" onclick="teste()" class="btn btn-default btn-lg btn-block">
                                                Atualizar carrinho
                                            </button>
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                            <div class="panel-footer">
                                <div class="row text-center">
                                    <div id="div_total" class="col-sm-9 col-xs-12">
                                        <h3 class="text-right">Total = <b>R$</b> <strong>${pedido == null ? "0.00" : pedido.valor_total}</strong></h3>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <button type="submit" class="btn btn-success btn-block btn-lg">
                                            Fechar Pedido
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div> 
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        });       
    });
    
    function removerProduto(codproduto)
    {
         $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxCarrinho&codproduto=" + codproduto + "&qtde=0&acao=remover",
            success: function(msg){	
                //Recarrega a pagina
                location.reload();								
            }
        });        
    }
    function atualizarQtde(codproduto)
    {
        var qtde = $("#qtde_"+codproduto).val();
        
        $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxCarrinho&codproduto=" + codproduto + "&qtde=" + qtde + "&acao=atualizar",
            success: function(msg){	
                $("#div_total").html(msg);								
            }
        });       
    }
 
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    /*function addCarrinho(codproduto)
    {
        var qtde = $("#qtde_"+codproduto).val();

        $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxCarrinho&codproduto=" + codproduto + "&qtde=" + qtde,
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        });	
    }*/
</script>