<%-- 
    Document   : testeChk
    Created on : 12/05/2015, 23:38:03
    Author     : luizdagoberto
--%>

<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <% 
            String[] bebidas = request.getParameterValues("bebidas");
            System.out.println(bebidas);
            
            if(bebidas != null) {
                for(String b : bebidas)
                {
                    int qtde = Integer.parseInt(request.getParameter("qtd_" + b));
                    out.println(b + " - Quantidade: " + qtde);
                }
            }
            else
                out.println("Nenhuma bebida selecionada!");           
        %>
    </body>
</html>
