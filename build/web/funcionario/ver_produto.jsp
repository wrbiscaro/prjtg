<%@page import="br.edu.fatec.model.Produto"%>
<%@page import="br.edu.fatec.controller.ProdutoDAO"%>
<%@page import="br.edu.fatec.model.Ingrediente"%>
<%@page import="br.edu.fatec.controller.IngredienteDAO"%>
<%@page import="br.edu.fatec.model.Estoque"%>
<%@page import="br.edu.fatec.controller.EstoqueDAO"%>
<%@page import="br.edu.fatec.model.Funcionario"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Funcionario f = (Funcionario)session.getAttribute("funcionario");
    
    Conexao conexao = new Conexao();
    ProdutoDAO produtoDao = new ProdutoDAO(conexao.conectar());
    ArrayList<Produto> produtos = produtoDao.listar();
    
    //Add no escopo de requisicao para ser acessado por EL
    request.setAttribute("produtos", produtos);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
            div.display{display:inline-table;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h3 style="text-align:center;">Gerenciamento de Produtos</h3>
            <div id="div_msg" style="text-align:center;color: red;">
                <h4>${msg}</h4>
            </div> 
           
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" align="center" style="width: 750px;">
                            <thead>
                                <tr style="text-align: center;">
                                    <th>Codigo do Produto</th>
                                    <th>Produto</th>
                                    <th>Unidade de Medida</th>
                                    <th>Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="p" items="${produtos}">
                                    <tr style="text-align: center;">
                                        <td>${p.cod}</td>
                                        <td>${p.descricao}</td>
                                        <td>${p.unidade}</td>
                                        <td><a href="alterar_produto.jsp?codproduto=${p.cod}" class="btn btn-primary">Editar</a>
                                            <input type="button" class="btn btn-danger" id="excluir" name="excluir" onclick="excluirProduto(${p.cod});" value="Excluir">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="../js/jquery-2.1.3.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){ 

    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    
    function excluirProduto(codproduto)
    {
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxProduto&acao=excluir&codproduto=" + codproduto,
            success: function(msg){	
                alert(msg);
                location.reload();
            }
        }); 
    }
</script>