<%@page import="br.edu.fatec.model.Estoque"%>
<%@page import="br.edu.fatec.controller.EstoqueDAO"%>
<%@page import="br.edu.fatec.model.Funcionario"%>
<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Funcionario f = (Funcionario)session.getAttribute("funcionario");
    
    Conexao conexao = new Conexao();
    EstoqueDAO estoqueDao = new EstoqueDAO(conexao.conectar());
    ArrayList<Estoque> estoque = estoqueDao.listarPorLoja(f.getLoja().getCod());
    
    //Add no escopo de requisicao para ser acessado por EL
    request.setAttribute("estoque", estoque);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
            div.display{display:inline-table;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h3 style="text-align:center;">Estoque da Loja ${funcionario.loja.cod} - ${funcionario.loja.endereco}</h3>
            <div id="div_msg" style="text-align:center;color: red;">
                <h4>${msg}</h4>
            </div> 
           
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" align="center" style="width: 750px;">
                            <thead>
                                <tr style="text-align: center;">
                                    <th>Ingrediente</th>
                                    <th>Quantidade Disponível</th>
                                    <th>Quantidade Mínima</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="e" items="${estoque}">
                                    <tr style="text-align: center;">
                                        <td>${e.ingrediente.descricao}</td>
                                        <td>${e.estoque_disponivel}</td>
                                        <td>${e.estoque_minimo}</td>
                                        <td><input type="button" class="btn btn-primary" id="editar" name="editar" onclick="editarEstoque(${e.ingrediente.cod});" value="Editar"></td>
                                    </tr>
                                    <tr id="tr_${e.ingrediente.cod}" style="display: none;">
                                        <td colspan="4">
                                            <center>
                                                <div class="display" style="width: 170px;">
                                                    Tipo de movimentação
                                                    <select class="form-control" id="tipo_${e.ingrediente.cod}" name="tipo_${e.ingrediente.cod}" style="width: 145px;" onchange="alterarMovimentacao(${e.ingrediente.cod})">
                                                        <option value="1">Entrada</option>
                                                        <option value="2">Saida</option>
                                                    </select>
                                                </div>
                                                <div class="display" style="width: 135px;">
                                                    Motivo
                                                    <select class="form-control" id="tipomovimento_${e.ingrediente.cod}" name="tipomovimento_${e.ingrediente.cod}" style="width: 100px;">
                                                        <option value="1">Compra</option>
                                                        <option value="2">Ajustes</option>
                                                        <option value="3">Outros</option>
                                                    </select>
                                                </div>
                                                <div class="display" style="width: 210px;">
                                                    Quantidade
                                                    <input class="form-control" type="text" id="qtde_${e.ingrediente.cod}" name="qtde_${e.ingrediente.cod}" style="width: 185px;">
                                                </div>                                                    
                                                <div class="display" style="width: 170px;">
                                                    <br/>
                                                    <input type="button" class="btn btn-primary" id="atualizar" name="atualizar" onclick="atualizarEstoque(${e.loja.cod}, ${e.ingrediente.cod});" value="Atualizar Estoque">
                                                </div>                               
                                                <div style="display: none;" id="div_carregando_${e.ingrediente.cod}">
                                                    <br/>
                                                    <img src="../img/carregando_azul.gif"></b>
                                                </div>
                                            </center>
                                        </td>                    
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="../js/jquery-2.1.3.js"></script>
        <script src="../js/bootstrap.js"></script>
        <script src="../js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){ 

    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
    
    function editarEstoque(codingrediente)
    {
        if($("#tr_" + codingrediente).is(':visible')){
            $("#tr_" + codingrediente).hide();
        } 
        else{
            $("#tr_" + codingrediente).show();
        }
    }
    
    function alterarMovimentacao(codingrediente)
    {
        if($("#tipo_" + codingrediente).val() == 1)
            $("#tipomovimento_" + codingrediente).html('<option value="1">Compra</option><option value="2">Ajustes</option><option value="3">Outros</option>');
        else
            $("#tipomovimento_" + codingrediente).html('<option value="7">Ajustes</option><option value="8">Outros</option>');
    }
    
    function atualizarEstoque(codloja, codingrediente)
    {
        $("#div_carregando_" + codingrediente).show();
        
        var tipomovimento = $("#tipomovimento_" + codingrediente).val();
        var qtde = $("#qtde_" + codingrediente).val();
        
        $("#div_msg").html("");
        
        $.ajax({
            type: "POST",
            url: "../Servlet",
            data: "classe=AjaxEstoque&acao=atualizarEstoque&codloja=" + codloja + "&codingrediente=" + codingrediente + "&tipomovimento=" + tipomovimento + "&qtde=" + qtde,
            success: function(msg){
                $("#div_carregando_" + codingrediente).hide();
                if(msg == 1)
                {
                    alert("Atualizado com sucesso!");
                    location.reload();
                }
                else
                    alert("Quantidade insuficiente em estoque!");
            }
        });
    }
</script>