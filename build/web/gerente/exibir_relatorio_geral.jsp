<%@page import="br.edu.fatec.model.Usuario"%>
<%@page import="br.edu.fatec.model.Pedido"%>
<%@page import="br.edu.fatec.controller.PedidoDAO"%>
<%@page import="br.edu.fatec.model.Cliente"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h3>Relatório de Faturamento Geral - ${datainicial} - ${datafinal}</h3>
            <div id="msg" style="text-align:center;color: red;">
                <h4>${msg}</h4>
            </div> 
            
            <input type="hidden" id="lojas" name="lojas" value="${lojas}">
            <input type="hidden" id="valores" name="valores" value="${valores}">
            
            <div class="box-chart">
                <canvas id="GraficoBarra" style="width:100%;"></canvas>
            </div>
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.maskedinput.js"></script>
        <script src="js/chart.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">                                                       
    $(document).ready(function(){
        //Valores buscados no bd ao carregar a pagina e armazenados em EL
        var lojas = $("#lojas").val().split("|");
        var valores = $("#valores").val().split("|");
        
        //Seta as opcoes do grafico
        var options = {
            responsive:true
        };

        //Seta os valores do grafico. Usar uma funcao javascript com ajax para pegar os dados do bd.
        var data = {       
            //labels: ["Avenida General Carneiro", "Avenida Itavuvu", "Avenida Antônio Carlos Comitre", "Avenida São Paulo"],
            labels: lojas,
            datasets: [
                {
                    label: "Dados primários",
                    fillColor: "rgba(74, 38, 255, 0.5)",
                    strokeColor: "rgba(74, 38, 255, 0.8)",
                    highlightFill: "rgba(74, 38, 255, 0.75)",
                    highlightStroke: "rgba(74, 38, 255, 1)",
                    //data: [5000.00, 6700.00, 9000.00, 3200.00]
                    data: valores
                }
                /*Adiciona uma segunda barra ao lado se for necessario. Nao esquecer da virgula abaixo para separar os datasets.
                ,{
                    label: "Dados secundários",
                    fillColor: "rgba(151,187,205,0.5)",
                    strokeColor: "rgba(151,187,205,0.8)",
                    highlightFill: "rgba(151,187,205,0.75)",
                    highlightStroke: "rgba(151,187,205,1)",
                    data: [28, 48, 40, 19, 86, 27, 90, 28, 28, 28, 28, 28]
                }*/
            ]
        }; 
    
        var ctx = document.getElementById("GraficoBarra").getContext("2d"); //Ou $("#GraficoBarra").get(0).getContext("2d"); com JQuery
        var BarChart = new Chart(ctx).Bar(data, options);
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
</script>