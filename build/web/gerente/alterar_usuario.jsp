<%@page import="br.edu.fatec.model.Usuario"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
    
    Conexao conexao = new Conexao();
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="http://localhost:8084/prjTG/css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <div class="row">
                <div class="col-sm-2 col-lg-2">
                </div>
                <form class="col-sm-8 col-lg-8" action="http://localhost:8084/prjTG/Servlet" method="POST">
                    <input type="hidden" name="classe" value="LogicaCliente" />
                    <input type="hidden" name="Cliente" value="CadastrarCompleto" readonly="readonly" />                  
                    
                    <legend>Dados pessoais</legend>
                    <div class="row">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" id="nome" name="nome" required>
                            </div>

                            <div class="form-group">
                                <label for="telefone">Telefone</label>
                                <input type="text" class="form-control" id="telefone" name="telefone" required>
                            </div>
                        </fieldset>
                        
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="dtnasc">Data de Nascimento</label>
                                <input type="text" class="form-control" id="dtnasc" name="dtnasc" required>
                            </div>
                            <div class="form-group">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" id="cpf" name="cpf" required>
                            </div>
                        </fieldset>
                    </div>
                    
                    <legend>Dados de acesso</legend>
                    <div class="row">
                        <fieldset class="col-md-6">
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="text" class="form-control" id="email" name="email" required>
                            </div>  
                            
                            <div class="form-group">
                                <label for="senha">Senha</label>
                                <input type="password" class="form-control" id="senha" name="senha">
                            </div>
                        </fieldset>
                        
                        <fieldset class="col-md-6">                        
                            <div class="form-group">
                                <label for="repeticaosenha">Repita a senha</label>
                                <input type="password" class="form-control" id="repeticaosenha" name="repeticaosenha">
                            </div>
                            
                            <div class="form-group">
                                <label for="perfil">Perfil</label>
                                <select class="form-control" id="perfil" name="perfil">
                                    <option value="1">Gerente</option>
                                    <option value="2">Funcionário</option>
                                </select>
                            </div>
                        </fieldset>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg pull-right" id="calcular">
                        <span class="glyphicon glyphicon-ok"></span>
                         Alterar
                    </button>
                </form>
                <div class="col-sm-2 col-lg-2">
                </div>
            </div>                           
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="http://localhost:8084/prjTG/js/jquery-2.1.3.js"></script>
        <script src="http://localhost:8084/prjTG/js/bootstrap.js"></script>
        <script src="http://localhost:8084/prjTG/js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){
         $.ajax({
            type: "POST",
            url: "http://localhost:8084/prjTG/Servlet",
            data: "classe=AjaxCarrinho&codproduto=0&qtde=0&acao=carregar",
            success: function(msg){	
                $("#div_carrinho").html(msg);								
            }
        });  
        
        $("#cep").mask("99999-999");
        $("#dtnasc").mask("99/99/9999");
        $("#telefone").mask("(99) 99999-9999");
    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "http://localhost:8084/prjTG/Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
</script>
