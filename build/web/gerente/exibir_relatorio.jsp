<%@page import="br.edu.fatec.model.Usuario"%>
<%@page import="br.edu.fatec.model.Pedido"%>
<%@page import="br.edu.fatec.controller.PedidoDAO"%>
<%@page import="br.edu.fatec.model.Cliente"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
    Document   : cardapio
    Created on : 09/03/2015, 23:07:34
    Author     : luizdagoberto
--%>

<%@page import="br.edu.fatec.controller.Conexao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.edu.fatec.model.HeadFoot"%>
<%@ page contentType="text/html; charset=ISO-8859-1" language="java" pageEncoding="UTF-8"%>

<% 
    Usuario usuario = (Usuario) session.getAttribute("usuario");
    int perfil = (usuario == null ? 0 : usuario.getPerfil().getCod());
    //System.out.println(perfil);
    HeadFoot hf = new HeadFoot(perfil);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>WB Delivery</title>
        <meta name="viewport" content="width=device-width">
        <style>
            body {margin-top: 60px;}
        </style>
    </head>
    <body>
        <!--Imprime o menu do topo-->
        <% out.print(hf.getHead()); %>

        <!-- Conteudo da pagina -->
        <div class="container">
            <h3>Relatório de Faturamento - ${datainicial} - ${datafinal}</h3>
            <div id="msg" style="text-align:center;color: red;">
                <h4>${msg}</h4>
            </div> 
            
            <c:if test="${pedidos != null}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" align="center" style="width: 750px;">
                                <thead>
                                    <tr style="text-align: center;">
                                        <th>Número do Pedido</th>
                                        <th>Data</th>
                                        <th>Forma de Pagamento</th>
                                        <th>Cliente</th>
                                        <th>Valor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="p" items="${pedidos}">
                                        <tr style="text-align: center;">
                                                    <td>${p.cod}</td>
                                                    <td>${p.data_hora}</td>
                                                    <td>${p.forma_pagamento == 1 ? "Dinheiro" : "Cartão"}</td>
                                                    <td>${p.cliente.nome}</td>
                                                    <td>R$ ${p.valor_total}</td>
                                        </tr>
                                    </c:forEach>
                                    <tr>
                                        <td colspan="5">&nbsp;</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td colspan="4"><b>Faturamento do Período</b></td>
                                        <td><b>R$ ${totalperiodo}</b></td>
                                    </tr>
                                </tbody>
                            </table>
                       </div>
                    </div> 
                </div>
            </c:if>
        </div>
        <br/><br/>
        
        <!--Imprime o rodapé-->
        <% out.print(hf.getFoot()); %>
        
        <script src="js/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.maskedinput.js"></script>
    </body>
</html>

<% //session.invalidate(); %>

<style>
    .navbar {
      margin: 0;
    }
</style>
    
<script type="text/javascript">
    $(document).ready(function(){

    });
    
    function logout()
    {
        $.ajax({
            type: "POST",
            url: "Servlet",
            data: "classe=AjaxLogin&acao=logout",
            success: function(msg){	
                								
            }
        });
    }
</script>