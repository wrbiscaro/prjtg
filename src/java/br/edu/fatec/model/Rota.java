/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.model;

//Imports do calculo de rota
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Normalizer;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 *
 * @author luizdagoberto
 */
public class Rota {
    public static double calcular(String origem, String destino) {
        URL url;
        try {
            url = new URL(
                    "http://maps.google.es/maps/api/directions/xml?origin="
                            + origem + "&destination=" + destino
                            + "&sensor=false");

            Document document = getDocumento(url);

            return analisaXml(document);
        } catch (MalformedURLException  e ) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
	}
        return -1;
    }

    @SuppressWarnings("rawtypes")
    public static double analisaXml(Document document) {
        List list = document
                .selectNodes("//DirectionsResponse/route/leg/distance/text");
        
        //Verifica se não foi encontrada a rota
        if(list.isEmpty()){
           return -1;
        }
        else{
            //System.out.println(list.size());
            Element element = (Element) list.get(list.size() - 1);
            //Tirando os caracteres alfanumericos e espaco.
            String strDistancia = element.getText().replace("k", "").replace("m", "").replace(",", ".").trim();
            //return element.getText();
            double distancia = Double.parseDouble(strDistancia);
            return distancia;
        }
    }

    public static Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    } 
    
    //Metodo para remover acentos
    public static String removerAcentos(String str) {
        return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
}

