/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author luizdagoberto
 */
public class Pedido {
    private int cod;
    private Cliente cliente;
    private BigDecimal valor_total;
    private int status;
    //private Calendar data_hora = Calendar.getInstance();
    private String data_hora;
    private String observacao;
    private BigDecimal taxa_entrega;
    private int forma_pedido;
    private Loja loja;
    private Endereco endereco;
    private ArrayList<ItemPedido> itempedido;
    private int forma_pagamento;
    private BigDecimal troco;
    
    public Pedido(int cod) {
        this.cod = cod;
        itempedido = new ArrayList<ItemPedido>();
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public BigDecimal getValor_total() {
        return valor_total;
    }

    public void setValor_total(BigDecimal valor_total) {
        this.valor_total = valor_total;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /*public Calendar getData_hora() {
        return data_hora;
    }

    public void setData_hora(Calendar data_hora) {
        this.data_hora = data_hora;
    }*/

    public String getData_hora() {
        return data_hora;
    }

    public void setData_hora(String data_hora) {
        this.data_hora = data_hora;
    }
    
    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public BigDecimal getTaxa_entrega() {
        return taxa_entrega;
    }

    public void setTaxa_entrega(BigDecimal taxa_entrega) {
        this.taxa_entrega = taxa_entrega;
    }

    public int getForma_pedido() {
        return forma_pedido;
    }

    public void setForma_pedido(int forma_pedido) {
        this.forma_pedido = forma_pedido;
    }

    public ArrayList<ItemPedido> getItempedido() {
        return itempedido;
    }

    public void setItempedido(ArrayList<ItemPedido> itempedido) {
        this.itempedido = itempedido;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public int getForma_pagamento() {
        return forma_pagamento;
    }

    public void setForma_pagamento(int forma_pagamento) {
        this.forma_pagamento = forma_pagamento;
    }

    public BigDecimal getTroco() {
        return troco;
    }

    public void setTroco(BigDecimal troco) {
        this.troco = troco;
    }
    
    
    //Verifica se um produto já está no pedido
    public Integer pesquisaProduto(int codProduto){
        //int indice = 0; //indice do produto no array
        
        for(ItemPedido ip : itempedido){
            Produto p = ip.getProduto();
            
            if(p.getCod() == codProduto)
                return itempedido.indexOf(ip);
            //else
                //indice++;
        }
        return null;
    }
}
