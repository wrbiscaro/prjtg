/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.model;

/**
 *
 * @author luizdagoberto
 */
public class HeadFoot {
    private String head;
    private String foot;
    private int perfil;
    private String url = "http://localhost:8084/prjTG/";

    public HeadFoot(int perfil) {
        this.perfil = perfil;
        foot = "<div class=\"footer\">" +
                        "<div class=\"col-xs-12 center\">" +
                            "<legend></legend>" +
                            "<p style=\"text-align: center;\">" +
                                 "WB Delivery - Pediu, chegou!<br/>" +
                                "<small>Copyright  ©2015, All Rights Reserved.</small>" +
                            "</p>" +
                        "</div>\n" +
                    "</div>";
    }

    public String getHead() {
        if(perfil == 1)
            head = " <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n" +
            "          <div class=\"navbar-header\">\n" +
            "            <a class=\"navbar-brand\" href=\"" + url + "index.jsp\"><span style=\"margin-right:5px;\" class=\"glyphicon glyphicon-home\"></span>WB Delivery</a>\n" +
            "            <button class=\"navbar-toggle\" type=\"button\" data-target=\".navbar-collapse\" data-toggle=\"collapse\">\n" +
            "                <span class=\"glyphicon glyphicon-align-justify\" style=\"color:gainsboro\"></span>\n" +
            "            </button>\n" +
            "          </div>\n" +
            "           <ul class=\"nav navbar-nav collapse navbar-collapse\">\n" +
            "               <ul class=\"nav navbar-nav\">" + 
            "                   <li class=\"dropdown \">" + 
            "                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                       <span class=\"glyphicon glyphicon-glass\"></span> &nbsp; Produtos" + 
            "                       <span class=\"caret\"></span></a>" + 
            "                       <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                           <li class=\"\"><a href=\"" + url + "funcionario/inserir_produto.jsp\"><span class=\"glyphicon glyphicon-plus\"></span> &nbsp; Cadastrar</a></li>" + 
            "                           <li class=\"\"><a href=\"" + url + "funcionario/ver_produto.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Gerenciar</a></li>" +                
            "                       </ul>" + 
            "                   </li>" + 
            "               </ul>" +
            "               <ul class=\"nav navbar-nav\">" + 
            "                   <li class=\"dropdown \">" + 
            "                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                       <span class=\"glyphicon glyphicon-leaf\"></span> &nbsp; Ingredientes" + 
            "                       <span class=\"caret\"></span></a>" + 
            "                       <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                           <li class=\"\"><a href=\"" + url + "funcionario/inserir_ingrediente.jsp\"><span class=\"glyphicon glyphicon-plus\"></span> &nbsp; Cadastrar</a></li>" + 
            "                           <li class=\"\"><a href=\"" + url + "funcionario/ver_ingrediente.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Gerenciar</a></li>" +               
            "                       </ul>" + 
            "                   </li>" + 
            "               </ul>" +
            "               <li><a href=\"" + url + "funcionario/gerenciar_pedidos.jsp\"><span class=\"glyphicon glyphicon-credit-card\"></span> &nbsp; Pedidos</a></li>\n" + 
            "               <li><a href=\"" + url + "funcionario/ver_estoque.jsp\"><span class=\"glyphicon glyphicon-list-alt\"></span> &nbsp; Estoque</a></li>\n" +  
            "               <ul class=\"nav navbar-nav\">" + 
            "                   <li class=\"dropdown \">" + 
            "                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                       <span class=\"glyphicon glyphicon-user\"></span> &nbsp; Usuários" + 
            "                       <span class=\"caret\"></span></a>" + 
            "                       <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                           <li class=\"\"><a href=\"" + url + "gerente/cadastrar_usuario.jsp\"><span class=\"glyphicon glyphicon-plus\"></span> &nbsp; Cadastrar</a></li>" + 
            "                           <li class=\"\"><a href=\"" + url + "gerente/gerenciar_usuarios.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Gerenciar</a></li>" +                
            "                       </ul>" + 
            "                   </li>" + 
            "               </ul>" +
            "               <ul class=\"nav navbar-nav\">" + 
            "                   <li class=\"dropdown \">" + 
            "                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                       <span class=\"glyphicon glyphicon-road\"></span> &nbsp; Unidades" + 
            "                       <span class=\"caret\"></span></a>" + 
            "                       <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                           <li class=\"\"><a href=\"" + url + "gerente/cadastrar_unidade.jsp\"><span class=\"glyphicon glyphicon-plus\"></span> &nbsp; Cadastrar</a></li>" + 
            "                           <li class=\"\"><a href=\"" + url + "gerente/gerenciar_unidades.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Gerenciar</a></li>" +                
            "                       </ul>" + 
            "                   </li>" + 
            "               </ul>" +
            "               <ul class=\"nav navbar-nav\">" + 
            "                   <li class=\"dropdown \">" + 
            "                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                       <span class=\"glyphicon glyphicon-usd\"></span> &nbsp; Relatórios" + 
            "                       <span class=\"caret\"></span></a>" + 
            "                       <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                           <li class=\"\"><a href=\"" + url + "gerente/periodo_relatorio.jsp?relatorio=1\"><span class=\"glyphicon glyphicon-resize-small\"></span> &nbsp; Minha Unidade</a></li>" + 
            "                           <li class=\"\"><a href=\"" + url + "gerente/periodo_relatorio.jsp?relatorio=2\"><span class=\"glyphicon glyphicon-resize-full\"></span> &nbsp; Geral</a></li>" +               
            "                       </ul>" + 
            "                   </li>" + 
            "               </ul>" +
            "           </ul>\n" +
            "           <ul class=\"nav navbar-nav navbar-right\" style=\"margin-right:5px;\">" + 
            "               <li class=\"dropdown \">" + 
            "                   <a href=\"#\" class=\"navbar-brand dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                   <span class=\"glyphicon glyphicon-user\"></span> &nbsp; Minha Conta" + 
            "                   <span class=\"caret\"></span></a>" + 
            "                   <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                       <li class=\"dropdown-header\">Menu</li>" + 
            "                       <li class=\"\"><a href=\"" + url + "funcionario/editar_dados_funcionario.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Dados Pessoais</a></li>" + 
            "                       <li class=\"divider\"></li>" + 
            "                       <li><a href=\"" + url + "index.jsp\" onclick=\"logout()\"><span class=\"glyphicon glyphicon-log-out\"></span> &nbsp; Logout</a></li>" + 
            "                   </ul>" + 
            "               </li>" + 
            "          </ul>" +
            "        </nav>";
        else if(perfil == 2)
            head = " <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n" +
            "          <div class=\"navbar-header\">\n" +
            "            <a class=\"navbar-brand\" href=\"" + url + "index.jsp\"><span style=\"margin-right:5px;\" class=\"glyphicon glyphicon-home\"></span>WB Delivery</a>\n" +
            "            <button class=\"navbar-toggle\" type=\"button\" data-target=\".navbar-collapse\" data-toggle=\"collapse\">\n" +
            "                <span class=\"glyphicon glyphicon-align-justify\" style=\"color:gainsboro\"></span>\n" +
            "            </button>\n" +
            "          </div>\n" +
            "           <ul class=\"nav navbar-nav collapse navbar-collapse\">\n" +
            "               <ul class=\"nav navbar-nav\">" + 
            "                   <li class=\"dropdown \">" + 
            "                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                       <span class=\"glyphicon glyphicon-glass\"></span> &nbsp; Produtos" + 
            "                       <span class=\"caret\"></span></a>" + 
            "                       <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                           <li class=\"\"><a href=\"" + url + "funcionario/inserir_produto.jsp\"><span class=\"glyphicon glyphicon-plus\"></span> &nbsp; Cadastrar</a></li>" + 
            "                           <li class=\"\"><a href=\"" + url + "funcionario/ver_produto.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Gerenciar</a></li>" +                
            "                       </ul>" + 
            "                   </li>" + 
            "               </ul>" +
            "               <ul class=\"nav navbar-nav\">" + 
            "                   <li class=\"dropdown \">" + 
            "                       <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                       <span class=\"glyphicon glyphicon-leaf\"></span> &nbsp; Ingredientes" + 
            "                       <span class=\"caret\"></span></a>" + 
            "                       <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                           <li class=\"\"><a href=\"" + url + "funcionario/inserir_ingrediente.jsp\"><span class=\"glyphicon glyphicon-plus\"></span> &nbsp; Cadastrar</a></li>" + 
            "                           <li class=\"\"><a href=\"" + url + "funcionario/ver_ingrediente.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Gerenciar</a></li>" +               
            "                       </ul>" + 
            "                   </li>" + 
            "               </ul>" +
            "               <li><a href=\"" + url + "funcionario/gerenciar_pedidos.jsp\"><span class=\"glyphicon glyphicon-credit-card\"></span> &nbsp; Pedidos</a></li>\n" + 
            "               <li><a href=\"" + url + "funcionario/ver_estoque.jsp\"><span class=\"glyphicon glyphicon-list-alt\"></span> &nbsp; Estoque</a></li>\n" +                      
            "           </ul>\n" +
            "           <ul class=\"nav navbar-nav navbar-right\" style=\"margin-right:5px;\">" + 
            "               <li class=\"dropdown \">" + 
            "                   <a href=\"#\" class=\"navbar-brand dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                   <span class=\"glyphicon glyphicon-user\"></span> &nbsp; Minha Conta" + 
            "                   <span class=\"caret\"></span></a>" + 
            "                   <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                       <li class=\"dropdown-header\">Menu</li>" + 
            "                       <li class=\"\"><a href=\"" + url + "funcionario/editar_dados_funcionario.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Dados Pessoais</a></li>" + 
            "                       <li class=\"divider\"></li>" + 
            "                       <li><a href=\"" + url + "index.jsp\" onclick=\"logout()\"><span class=\"glyphicon glyphicon-log-out\"></span> &nbsp; Logout</a></li>" + 
            "                   </ul>" + 
            "               </li>" + 
            "          </ul>" +
            "        </nav>";
        else if(perfil == 3)
            head = " <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n" +
            "          <div class=\"navbar-header\">\n" +
            "            <a class=\"navbar-brand\" href=\"" + url + "index.jsp\"><span style=\"margin-right:5px;\" class=\"glyphicon glyphicon-home\"></span>WB Delivery</a>\n" +
            "            <button class=\"navbar-toggle\" type=\"button\" data-target=\".navbar-collapse\" data-toggle=\"collapse\">\n" +
            "                <span class=\"glyphicon glyphicon-align-justify\" style=\"color:gainsboro\"></span>\n" +
            "            </button>\n" +
            "          </div>\n" +
            "           <ul class=\"nav navbar-nav collapse navbar-collapse\">\n" +
            "               <li><a href=\"" + url + "cardapio.jsp\"><span class=\"glyphicon glyphicon-cutlery\"></span> &nbsp; Cardápio</a></li>\n" + 
            "               <li><a href=\"" + url + "cliente/meus_pedidos.jsp\"><span class=\"glyphicon glyphicon-credit-card\"></span> &nbsp; Meus Pedidos</a></li>\n" + 
            "           </ul>\n" +
            "           <ul class=\"nav navbar-nav navbar-right\" style=\"margin-right:5px;\">" + 
            "               <li class=\"dropdown \">" + 
            "                   <a href=\"#\" class=\"navbar-brand dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                   <span class=\"glyphicon glyphicon-user\"></span> &nbsp; Minha Conta" + 
            "                   <span class=\"caret\"></span></a>" + 
            "                   <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                       <li class=\"dropdown-header\">Menu</li>" + 
            "                       <li class=\"\"><a href=\"" + url + "cliente/editar_dados_cliente.jsp\"><span class=\"glyphicon glyphicon-pencil\"></span> &nbsp; Dados Pessoais</a></li>" + 
            "                       <li class=\"\"><a href=\"" + url + "cliente/gerenciar_endereco.jsp\"><span class=\"glyphicon glyphicon-list-alt\"></span> &nbsp; Endereços</a></li>" + 
            "                       <li class=\"divider\"></li>" + 
            "                       <li><a href=\"" + url + "index.jsp\" onclick=\"logout()\"><span class=\"glyphicon glyphicon-log-out\"></span> &nbsp; Logout</a></li>" + 
            "                   </ul>" + 
            "               </li>" + 
            "          </ul>" +
            "          <div id=\"div_carrinho\" style=\"float:right;margin-right:10px;\">\n" +
            "          </div>" +
            "        </nav>";
        else
            head = " <nav class=\"navbar navbar-inverse navbar-fixed-top\">\n" +
            "          <div class=\"navbar-header\">\n" +
            "            <a class=\"navbar-brand\" href=\"" + url + "index.jsp\"><span style=\"margin-right:5px;\" class=\"glyphicon glyphicon-home\"></span>WB Delivery</a>\n" +
            "            <button class=\"navbar-toggle\" type=\"button\" data-target=\".navbar-collapse\" data-toggle=\"collapse\">\n" +
            "                <span class=\"glyphicon glyphicon-align-justify\" style=\"color:gainsboro\"></span>\n" +
            "            </button>\n" +
            "          </div>\n" +
            "           <ul class=\"nav navbar-nav collapse navbar-collapse\">\n" +
            "               <!--<li><a href=\"sobre.php\">Sobre</a></li>\n" +
            "               <li><a href=\"#\">Ajuda</a></li>\n" +
            "               <li><a href=\"#\">Perguntas frequentes</a></li>\n" +
            "               <li><a href=\"#\">Entre em contato</a></li>\n-->" + 
            "               <li><a href=\"" + url + "cardapio.jsp\"><span class=\"glyphicon glyphicon-cutlery\"></span> &nbsp; Cardápio</a></li>\n" + 
            "           </ul>\n" +
            "           <ul class=\"nav navbar-nav navbar-right\" style=\"margin-right:5px;\">" + 
            "               <li class=\"dropdown \">" + 
            "                   <a href=\"#\" class=\"navbar-brand dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">" + 
            "                   <span class=\"glyphicon glyphicon-user\"></span> &nbsp; Minha Conta" + 
            "                   <span class=\"caret\"></span></a>" + 
            "                   <ul class=\"dropdown-menu\" role=\"menu\">" + 
            "                       <li class=\"\"><a href=\"" + url + "login.jsp\"><span class=\"glyphicon glyphicon-log-in\"></span> &nbsp; Entrar</a></li>" + 
            "                   </ul>" + 
            "               </li>" + 
            "          </ul>" +
            "          <div id=\"div_carrinho\" style=\"float:right;margin-right:10px;\">\n" +
            "          </div>" +
            "        </nav>";
        
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    public int getPerfil() {
        return perfil;
    }

    public void setPerfil(int perfil) {
        this.perfil = perfil;
    }
}
