/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.model;

import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class Ingrediente {
    private int cod;
    private String descricao;
    private String unidade;
    private Familia familia;
    private double qtde_padrao;
    private String estoque;
    //private ArrayList<Produto> produto;

    public Ingrediente(int cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
        //produto = new ArrayList<Produto>();
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public double getQtde_padrao() {
        return qtde_padrao;
    }

    public void setQtde_padrao(double qtde_padrao) {
        this.qtde_padrao = qtde_padrao;
    }

    /*public ArrayList<Produto> getProduto() {
    return produto;
    }
    public void setProduto(ArrayList<Produto> produto) {
    this.produto = produto;
    }*/ 
    
    public Familia getFamilia() {
        return familia;
    }

    public void setFamilia(Familia familia) {
        this.familia = familia;
    }  

    public String getEstoque() {
        return estoque;
    }

    public void setEstoque(String estoque) {
        this.estoque = estoque;
    }
}
