/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.model;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class Produto {
    private int cod;
    private String descricao;
    //private Image foto;
    private BigDecimal preco;
    private String unidade;
    private String categoria;
    private ArrayList<Ingrediente> ingrediente;

    public Produto(int cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
        ingrediente = new ArrayList<Ingrediente>();
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public ArrayList<Ingrediente> getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(ArrayList<Ingrediente> ingrediente) {
        this.ingrediente = ingrediente;
    }
}
