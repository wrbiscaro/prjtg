/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.model;

/**
 *
 * @author luizdagoberto
 */
public class Estoque {
    private Loja loja;
    private Ingrediente ingrediente;
    private double estoque_disponivel;
    private double estoque_minimo;

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Ingrediente getIngrediente() {
        return ingrediente;
    }

    public void setIngrediente(Ingrediente ingrediente) {
        this.ingrediente = ingrediente;
    }

    public double getEstoque_disponivel() {
        return estoque_disponivel;
    }

    public void setEstoque_disponivel(double estoque_disponivel) {
        this.estoque_disponivel = estoque_disponivel;
    }

    public double getEstoque_minimo() {
        return estoque_minimo;
    }

    public void setEstoque_minimo(double estoque_minimo) {
        this.estoque_minimo = estoque_minimo;
    }
}
