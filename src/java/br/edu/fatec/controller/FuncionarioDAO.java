/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Funcionario;
import br.edu.fatec.model.Loja;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author luizdagoberto
 */
public class FuncionarioDAO {
    private Connection conn;

    public FuncionarioDAO(Connection conn) {
        this.conn = conn;
    }
    
    public Funcionario selecionarPorUsuario(int codusuario){
        Funcionario f = null;    
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT cod_funcionario, nome_funcionario, cpf_funcionario, telefone_funcionario, DATE_FORMAT(datanasc_funcionario, '%d/%m/%Y') AS datanasc_funcionariof, cod_usuario, cod_loja FROM tbfuncionario WHERE cod_usuario = ?");
            ps.setInt(1, codusuario);
            
            ResultSet rs = ps.executeQuery();

            if(rs.first()){
                f = new Funcionario(rs.getInt("cod_funcionario"), rs.getString("nome_funcionario"));
                f.setCpf(rs.getString("cpf_funcionario"));
                f.setTelefone(rs.getString("telefone_funcionario"));
                f.setDataNasc(rs.getString("datanasc_funcionariof"));
                
                ps = conn.prepareStatement("SELECT * FROM tbloja WHERE cod_loja = ?");
                ps.setInt(1, rs.getInt("cod_loja"));
                
                ResultSet rs2 = ps.executeQuery();
                
                if(rs2.first()){
                    Loja l = new Loja(rs2.getInt("cod_loja"), rs2.getString("cep_loja"));
                    l.setEndereco(rs2.getString("endereco_loja"));
                    l.setNumero(rs2.getInt("numero_loja"));
                    l.setBairro(rs2.getString("bairro_loja"));
                    l.setCidade(rs2.getString("cidade_loja"));
                    l.setUf(rs2.getString("uf_loja"));
                    l.setTelefone(rs2.getString("telefone_loja"));
                    l.setEmail(rs2.getString("email_loja"));
                    l.setComplemento(rs2.getString("complemento_loja"));
                    
                    f.setLoja(l);
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return f;
    } 
    
    public void alterar(Funcionario f){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbfuncionario SET nome_funcionario = ?, telefone_funcionario = ?, datanasc_funcionario = ?, cpf_funcionario = ? WHERE cod_funcionario = ?");
            ps.setString(1, f.getNome());
            ps.setString(2, f.getTelefone());
            ps.setString(3, f.getDataNasc());
            ps.setString(4, f.getCpf());
            ps.setInt(5, f.getCod());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }
    
    public int inserir(Funcionario f){
        int codfuncionario = 0;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbfuncionario (nome_funcionario, telefone_funcionario, datanasc_funcionario, cpf_funcionario, cod_usuario, cod_loja) VALUES(?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, f.getNome());
            ps.setString(2, f.getTelefone());
            ps.setString(3, f.getDataNasc());
            ps.setString(4, f.getCpf());
            ps.setInt(5, f.getUsuario().getCod());
            ps.setInt(6, f.getLoja().getCod());
            
            ps.execute();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next())
                codfuncionario = rs.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return codfuncionario;
    }
    
    public void excluir(int codfuncionario){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbfuncionario WHERE cod_funcionario = ?");
            ps.setInt(1, codfuncionario);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
    
    public void excluirPorLoja(int codloja){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbfuncionario WHERE cod_loja = ?");
            ps.setInt(1, codloja);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
}
