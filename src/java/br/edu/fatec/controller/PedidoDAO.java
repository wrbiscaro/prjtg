/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Cliente;
import br.edu.fatec.model.Endereco;
import br.edu.fatec.model.ItemPedido;
import br.edu.fatec.model.Pedido;
import br.edu.fatec.model.Produto;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class PedidoDAO {
    private Connection conn;

    public PedidoDAO(Connection conn) {
        this.conn = conn;
    }
    
    public int inserir(Pedido p){
        int codpedido = 0;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbpedido (cod_cliente, valor_total, status_pedido, observacao, taxa_entrega, forma_pedido, forma_pagamento, troco, cod_loja, cod_endereco, data_hora) VALUES(?,?,?,?,?,?,?,?,?,?,NOW())", PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setInt(1, p.getCliente().getCod());
            ps.setBigDecimal(2, p.getValor_total());
            ps.setInt(3, 1);
            ps.setString(4, p.getObservacao());
            ps.setDouble(5, 2.50);
            ps.setInt(6, 1);
            ps.setInt(7, p.getForma_pagamento());
            ps.setBigDecimal(8, p.getTroco());
            ps.setInt(9, p.getLoja().getCod());
            ps.setInt(10, p.getEndereco().getCod());
            
            ps.execute();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next())
                codpedido = rs.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return codpedido;  
    }
    
    public ArrayList<Pedido> listarPorCliente(int codcliente){
        ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT cod_pedido, valor_total, status_pedido, DATE_FORMAT(data_hora, '%d/%m/%Y %T') AS data_horaf, observacao, taxa_entrega, cod_endereco, forma_pagamento, troco FROM tbpedido WHERE cod_cliente = ? ORDER BY data_hora DESC;");
            ps.setInt(1, codcliente);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){ 
                Pedido p = new Pedido(rs.getInt("cod_pedido"));
                p.setValor_total(rs.getBigDecimal("valor_total"));
                p.setStatus(rs.getInt("status_pedido"));
                p.setData_hora(rs.getString("data_horaf"));
                p.setObservacao(rs.getString("observacao"));
                p.setTaxa_entrega(rs.getBigDecimal("taxa_entrega"));
                p.setEndereco(new Endereco(rs.getInt("cod_endereco"), null));
                p.setForma_pagamento(rs.getInt("forma_pagamento"));
                p.setTroco(rs.getBigDecimal("troco"));
                
                ps = conn.prepareStatement("SELECT * FROM tbitem_pedido WHERE cod_pedido = ?");
                ps.setInt(1, p.getCod());
                
                ResultSet rs2 = ps.executeQuery();
                
                while(rs2.next()){
                    ItemPedido ip = new ItemPedido();
                    ip.setPedido(p.getCod());
                    ip.setQtde(rs2.getInt("qtde_item_pedido"));
                    ip.setValor(rs2.getBigDecimal("valor_item_pedido"));
                    
                    ps = conn.prepareStatement("SELECT * FROM tbproduto WHERE cod_produto = ?");
                    ps.setInt(1, rs2.getInt("cod_produto"));
                    
                    ResultSet rs3 = ps.executeQuery();
                    
                    while(rs3.next()){
                        Produto produto = new Produto(rs3.getInt("cod_produto"), rs3.getString("descricao_produto"));
                        produto.setPreco(rs3.getBigDecimal("preco_produto"));
                        
                        ip.setProduto(produto);
                    }
                    
                    p.getItempedido().add(ip);
                }
                pedidos.add(p);
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return pedidos;
    } 
    
    public ArrayList<Pedido> listarPorLoja(int codloja){
        ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT cod_pedido, valor_total, status_pedido, DATE_FORMAT(data_hora, '%d/%m/%Y %T') AS data_horaf, observacao, taxa_entrega, cod_endereco, forma_pagamento, troco FROM tbpedido WHERE cod_loja = ? ORDER BY data_hora DESC;");
            ps.setInt(1, codloja);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){ 
                Pedido p = new Pedido(rs.getInt("cod_pedido"));
                p.setValor_total(rs.getBigDecimal("valor_total"));
                p.setStatus(rs.getInt("status_pedido"));
                p.setData_hora(rs.getString("data_horaf"));
                p.setObservacao(rs.getString("observacao"));
                p.setTaxa_entrega(rs.getBigDecimal("taxa_entrega"));
                p.setEndereco(new Endereco(rs.getInt("cod_endereco"), null));
                p.setForma_pagamento(rs.getInt("forma_pagamento"));
                p.setTroco(rs.getBigDecimal("troco"));
                
                ps = conn.prepareStatement("SELECT * FROM tbitem_pedido WHERE cod_pedido = ?");
                ps.setInt(1, p.getCod());
                
                ResultSet rs2 = ps.executeQuery();
                
                while(rs2.next()){
                    ItemPedido ip = new ItemPedido();
                    ip.setPedido(p.getCod());
                    ip.setQtde(rs2.getInt("qtde_item_pedido"));
                    ip.setValor(rs2.getBigDecimal("valor_item_pedido"));
                    
                    ps = conn.prepareStatement("SELECT * FROM tbproduto WHERE cod_produto = ?");
                    ps.setInt(1, rs2.getInt("cod_produto"));
                    
                    ResultSet rs3 = ps.executeQuery();
                    
                    while(rs3.next()){
                        Produto produto = new Produto(rs3.getInt("cod_produto"), rs3.getString("descricao_produto"));
                        produto.setPreco(rs3.getBigDecimal("preco_produto"));
                        
                        ip.setProduto(produto);
                    }
                    
                    p.getItempedido().add(ip);
                }
                pedidos.add(p);
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return pedidos;
    } 
    
    public boolean atualizarStatus(int status, int codpedido){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbpedido SET status_pedido = ? WHERE cod_pedido = ?");
            ps.setInt(1, status);
            ps.setInt(2, codpedido);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }        
        
        return true;  
    }
    
    public ArrayList<Pedido> relatorioPorUnidade(String data_inicial, String data_final, int codloja){
        ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT p.cod_pedido, (p.valor_total + p.taxa_entrega) AS valor_final, DATE_FORMAT(p.data_hora, '%d/%m/%Y %T') AS data_horaf, p.forma_pagamento, c.cod_cliente, c.nome_cliente\n" +
                                        "FROM tbpedido p\n" +
                                        "INNER JOIN tbcliente c ON p.cod_cliente = c.cod_cliente\n" +
                                        "WHERE p.status_pedido = 7 AND p.cod_loja = ? AND DATE_FORMAT(p.data_hora, '%Y-%m-%d') >= ? AND DATE_FORMAT(p.data_hora, '%Y-%m-%d') <= ? ORDER BY p.data_hora");
            ps.setInt(1, codloja);
            ps.setString(2, data_inicial);
            ps.setString(3, data_final);

            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Pedido p = new Pedido(rs.getInt("cod_pedido"));
                p.setValor_total(rs.getBigDecimal("valor_final"));
                p.setData_hora(rs.getString("data_horaf"));
                p.setForma_pagamento(rs.getInt("forma_pagamento"));
                p.setCliente(new Cliente(rs.getInt("cod_cliente"), rs.getString("nome_cliente")));
                
                pedidos.add(p);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }

        return pedidos;   
    }
    
    public BigDecimal relatorioTotalPorUnidade(String data_inicial, String data_final, int codloja){
        BigDecimal total = BigDecimal.ZERO;
        
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT SUM(valor_total) AS valor_total\n" +
                                       "FROM tbpedido\n" +
                                       "WHERE DATE(data_hora) >= ? AND DATE(data_hora) <= ? AND cod_loja = ?");
            
            ps.setString(1, data_inicial);
            ps.setString(2, data_final);
            ps.setInt(3, codloja);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()){
                if(rs.getBigDecimal("valor_total") != null)      
                    total = total.add(rs.getBigDecimal("valor_total"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }

        return total;   
    }
}
