/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Familia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class FamiliaDAO {
    private Connection conn;

    public FamiliaDAO(Connection conn) {
        this.conn = conn;
    } 
    
    public ArrayList<Familia> listar(){
        ArrayList<Familia> familias = new ArrayList<Familia>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbfamilia");

            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Familia f = new Familia(rs.getInt("cod_familia"), rs.getString("descricao_familia"));
                
                familias.add(f);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }

        return familias;   
    }
}
