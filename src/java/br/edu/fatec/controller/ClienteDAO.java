/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Cliente;
import br.edu.fatec.model.Usuario;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author luizdagoberto
 */
public class ClienteDAO {
    private Connection conn;

    public ClienteDAO(Connection conn) {
        this.conn = conn;
    }
    
    public int inserir(Cliente c){
        int codcliente = 0;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbcliente (nome_cliente, telefone_cliente, datanasc_cliente, cod_usuario) VALUES(?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, c.getNome());
            ps.setString(2, c.getTelefone());
            ps.setString(3, c.getDataNasc());
            ps.setInt(4, c.getUsuario().getCod());
            
            ps.execute();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next())
                codcliente = rs.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return codcliente;  
    }
    
    public Cliente selecionarPorUsuario(int codusuario){
        Cliente c = null;    
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT cod_cliente, nome_cliente, telefone_cliente, DATE_FORMAT(datanasc_cliente, '%d/%m/%Y') AS datanasc_clientef FROM tbcliente WHERE cod_usuario = ?");
            ps.setInt(1, codusuario);
            
            ResultSet rs = ps.executeQuery();

            if(rs.first()){
                c = new Cliente(rs.getInt("cod_cliente"), rs.getString("nome_cliente"));
                c.setTelefone(rs.getString("telefone_cliente"));
                c.setDataNasc(rs.getString("datanasc_clientef"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return c;
    }
    
    public Cliente selecionar(int codcliente){
        Cliente c = null;  
        Usuario u = null;
        
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT c.nome_cliente, c.telefone_cliente, DATE_FORMAT(c.datanasc_cliente, '%d/%m/%Y') AS datanasc_clientef, c.cod_usuario, u.email_usuario, u.senha_usuario "
                                     + "FROM tbcliente c INNER JOIN tbusuario u ON c.cod_usuario = u.cod_usuario WHERE c.cod_cliente = ?");
            ps.setInt(1, codcliente);
            
            ResultSet rs = ps.executeQuery();

            if(rs.first()){
                u = new Usuario(rs.getInt("cod_usuario"), rs.getString("email_usuario"), rs.getString("senha_usuario"));
                
                c = new Cliente(codcliente, rs.getString("nome_cliente"));
                c.setTelefone(rs.getString("telefone_cliente"));
                c.setDataNasc(rs.getString("datanasc_clientef"));
                c.setUsuario(u);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return c;
    }
    
    public void alterar(Cliente c){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbcliente SET nome_cliente = ?, telefone_cliente = ?, datanasc_cliente = ? WHERE cod_cliente = ?");
            ps.setString(1, c.getNome());
            ps.setString(2, c.getTelefone());
            ps.setString(3, c.getDataNasc());
            ps.setInt(4, c.getCod());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }
    
    public void excluir(int codcliente){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbcliente WHERE cod_cliente = ?");
            ps.setInt(1, codcliente);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
}
