/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Familia;
import br.edu.fatec.model.Ingrediente;
import br.edu.fatec.model.Loja;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author luizdagoberto
 */
public class LogicaIngrediente extends ClasseAbstrata {
    Conexao conexao = null;
    IngredienteDAO ingredienteDao = null;
    EstoqueDAO estoqueDao = null;
    LojaDAO lojaDao = null;
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        RequestDispatcher rd = null;
        conexao = new Conexao();
        ingredienteDao = new IngredienteDAO(conexao.conectar());

        String opcao = req.getParameter("Ingrediente");   
        
        if(opcao.equals("Cadastrar")){
            this.cadastrar(req, resp);
            req.setAttribute("msg", "Ingrediente cadastrado com sucesso!");
            
            rd = req.getRequestDispatcher("funcionario/inserir_ingrediente.jsp");  
        }
        else if(opcao.equals("Alterar")){
            if(this.alterar(req, resp))
                req.setAttribute("msg", "Ingrediente alterado com sucesso!");
            else
                req.setAttribute("msg", "Erro ao alterar ingrediente!");
            
            rd = req.getRequestDispatcher("funcionario/alterar_ingrediente.jsp");  
        }
        
        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(LogicaIngrediente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogicaIngrediente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cadastrar(HttpServletRequest req, HttpServletResponse resp){
        String descricao = req.getParameter("descricao");
        String unidade = req.getParameter("unidade");
        int familia = Integer.parseInt(req.getParameter("familia"));
        //double qtde_padrao = Double.parseDouble(req.getParameter("qtde_padrao"));
        String estoque = req.getParameter("estoque");
        
        Ingrediente i = new Ingrediente(0, descricao);
        i.setUnidade(unidade);
        i.setFamilia(new Familia(familia, null));
        //i.setQtde_padrao(qtde_padrao);
        
        if(estoque != null){
            estoqueDao = new EstoqueDAO(conexao.conectar());
            lojaDao = new LojaDAO(conexao.conectar());        
            ArrayList<Loja> lojas = lojaDao.listar();
            
            double qtde_padrao = Double.parseDouble(req.getParameter("qtde_padrao"));
            double estoque_minimo = Double.parseDouble(req.getParameter("qtde_minima"));
            i.setQtde_padrao(qtde_padrao);
            i.setEstoque("S");       
            
            int codingrediente = ingredienteDao.inserir(i);

            for(Loja l : lojas){
                estoqueDao.inserir(l.getCod(), codingrediente, estoque_minimo);
            }
        }
        else{
            i.setQtde_padrao(0);
            i.setEstoque("N");
            ingredienteDao.inserir(i);
        }
    }
    
    public boolean alterar(HttpServletRequest req, HttpServletResponse resp){
        estoqueDao = new EstoqueDAO(conexao.conectar());
        lojaDao = new LojaDAO(conexao.conectar());        
        ArrayList<Loja> lojas = lojaDao.listar();
            
        int codingrediente = Integer.parseInt(req.getParameter("codingrediente"));
        String descricao = req.getParameter("descricao");
        String unidade = req.getParameter("unidade");
        int familia = Integer.parseInt(req.getParameter("familia"));
        //double qtde_padrao = Double.parseDouble(req.getParameter("qtde_padrao"));
        String estoque = req.getParameter("estoque");
        
        Ingrediente i = new Ingrediente(codingrediente, descricao);
        i.setUnidade(unidade);
        i.setFamilia(new Familia(familia, null));
        //i.setQtde_padrao(qtde_padrao);
        
        if(estoque != null){
            double qtde_padrao = Double.parseDouble(req.getParameter("qtde_padrao"));
            double estoque_minimo = Double.parseDouble(req.getParameter("qtde_minima"));
            i.setQtde_padrao(qtde_padrao);
            i.setEstoque("S");       
            
            /*return ingredienteDao.alterar(i);*/

            for(Loja l : lojas){
                estoqueDao.atualizarQuantidadeMinima(l.getCod(), codingrediente, estoque_minimo);
                //estoqueDao.excluir(l.getCod(), codingrediente);
                //estoqueDao.inserir(l.getCod(), codingrediente, estoque_minimo);
            }
        }
        else{
            i.setQtde_padrao(0);
            i.setEstoque("N");
            
            for(Loja l : lojas){
                estoqueDao.excluir(l.getCod(), codingrediente);
            }
            //return ingredienteDao.alterar(i);
        }
        return ingredienteDao.alterar(i);
    }
}
