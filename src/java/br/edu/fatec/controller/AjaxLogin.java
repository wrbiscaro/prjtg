/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class AjaxLogin extends ClasseAbstrata {

    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession sessao = req.getSession();
        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("logout")){
                sessao.invalidate();
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }
 
    }
    
}
