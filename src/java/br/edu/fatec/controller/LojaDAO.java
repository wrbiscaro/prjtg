/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Loja;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class LojaDAO {
    private Connection conn;

    public LojaDAO(Connection conn) {
        this.conn = conn;
    }
    
    public ArrayList<Loja> listar(){
        ArrayList<Loja> lojas = new ArrayList<Loja>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbloja");
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Loja l = new Loja(rs.getInt("cod_loja"), rs.getString("cep_loja"));
                l.setEndereco(rs.getString("endereco_loja"));
                l.setNumero(rs.getInt("numero_loja"));
                l.setBairro(rs.getString("bairro_loja"));
                l.setCidade(rs.getString("cidade_loja"));
                l.setUf(rs.getString("uf_loja"));
                l.setTelefone(rs.getString("telefone_loja"));
                l.setEmail(rs.getString("email_loja"));
                l.setComplemento(rs.getString("complemento_loja"));
                
                lojas.add(l);
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return lojas;
    }  
    
    public int inserir(Loja l){
        int codloja = 0;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbloja (cep_loja, endereco_loja, numero_loja, bairro_loja, cidade_loja, uf_loja, complemento_loja, telefone_loja, email_loja) VALUES(?,?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, l.getCep());
            ps.setString(2, l.getEndereco());
            ps.setInt(3, l.getNumero());
            ps.setString(4, l.getBairro());
            ps.setString(5, l.getCidade());
            ps.setString(6, l.getUf());
            ps.setString(7, l.getComplemento());
            ps.setString(8, l.getTelefone());
            ps.setString(9, l.getEmail());
            
            ps.execute();

            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next())
                codloja = rs.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return codloja;  
    }
    
    public Loja selecionar(int codloja){
        Loja l = null;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbloja WHERE cod_loja = ?");
            ps.setInt(1, codloja);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                l = new Loja(rs.getInt("cod_loja"), rs.getString("cep_loja"));
                l.setEndereco(rs.getString("endereco_loja"));
                l.setNumero(rs.getInt("numero_loja"));
                l.setBairro(rs.getString("bairro_loja"));
                l.setCidade(rs.getString("cidade_loja"));
                l.setUf(rs.getString("uf_loja"));
                l.setTelefone(rs.getString("telefone_loja"));
                l.setEmail(rs.getString("email_loja"));
                l.setComplemento(rs.getString("complemento_loja"));
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return l;
    } 
    
    public boolean alterar(Loja l){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbloja SET cep_loja = ?, endereco_loja = ?, numero_loja = ?, bairro_loja = ?, cidade_loja = ?, uf_loja = ?, complemento_loja = ?, telefone_loja = ?, email_loja = ? WHERE cod_loja = ?");
            ps.setString(1, l.getCep());
            ps.setString(2, l.getEndereco());
            ps.setInt(3, l.getNumero());
            ps.setString(4, l.getBairro());
            ps.setString(5, l.getCidade());
            ps.setString(6, l.getUf());
            ps.setString(7, l.getComplemento());
            ps.setString(8, l.getTelefone());
            ps.setString(9, l.getEmail());
            ps.setInt(10, l.getCod());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }        
        
        return true;  
    }
    
    public void excluir(int codloja){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbloja WHERE cod_loja = ?");
            ps.setInt(1, codloja);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
}
