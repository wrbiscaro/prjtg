/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Cliente;
import br.edu.fatec.model.Endereco;
import br.edu.fatec.model.Loja;
import br.edu.fatec.model.Rota;
import br.edu.fatec.model.Usuario;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class LogicaEndereco extends ClasseAbstrata {
    Conexao conexao = null;
    EnderecoDAO enderecoDao = null;
    LojaDAO lojaDao = null;
    Endereco endereco = null;
    Rota r = new Rota();
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        RequestDispatcher rd = null;
        conexao = new Conexao();
        enderecoDao = new EnderecoDAO(conexao.conectar());
        
        String opcao = req.getParameter("Endereco");
        
        if(opcao.equals("Consultar")){
            boolean entrega = this.consultar(req, resp);
            
            if(entrega){
                req.setAttribute("msg", "Endereço atendido por nossas lojas! Complete o cadastro abaixo.");
                rd = req.getRequestDispatcher("cadastrar_cliente.jsp");            
            }
            else{
                req.setAttribute("msg", "Endereço não atendido por nossas lojas!");
                rd = req.getRequestDispatcher("cadastrar_endereco.jsp");                
            }
                
        }
        else if(opcao.equals("Cadastrar")){
            boolean cadastrado = this.cadastrar(req, resp);
            
            if(cadastrado)
                req.setAttribute("msg", "Endereço cadastrado com sucesso!");
            else
                req.setAttribute("msg", "Erro ao cadastrar o endereço! Verifique os dados informados.");           

            rd = req.getRequestDispatcher("cliente/cadastrar_endereco.jsp"); 
        }
        else if(opcao.equals("Alterar")){
            boolean alterado = this.alterar(req, resp);
            
            if(alterado)
                req.setAttribute("msg", "Endereço alterado com sucesso!");
            else
                req.setAttribute("msg", "Erro ao alterar o endereço! Verifique os dados informados.");           

            rd = req.getRequestDispatcher("cliente/alterar_endereco.jsp"); 
        }
        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(LogicaEndereco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogicaEndereco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean consultar(HttpServletRequest req, HttpServletResponse resp){
        Endereco endereco;
        
        lojaDao = new LojaDAO(conexao.conectar());
        ArrayList<Loja> unidades = lojaDao.listar();
        
        String cep = req.getParameter("cep");    
        String arrayCep[] = cep.split("-");
        cep = arrayCep[0] + arrayCep[1];
        
        endereco = enderecoDao.pesquisarPorCEP(cep);
        
        if(endereco == null)
            return false;
        else{
            double distancia, menorDistancia = 999;
            String strEndereco = endereco.getEndereco() + ", " + endereco.getCidade() + " - " + endereco.getUf();
            
            strEndereco = r.removerAcentos(strEndereco);
            System.out.println(strEndereco);
          
            for(Loja u : unidades){
                String loja = u.getEndereco() + ", " + u.getCidade() + " - " + u.getUf();
                loja = r.removerAcentos(loja);
                
                distancia = r.calcular(loja, strEndereco);
                
                if(distancia == -1)
                    System.out.println("Não foi possivel calcular a rota para a unidade " + u);
                else
                {
                    System.out.println("Distancia para a unidade " + loja + ": " + distancia);
                    
                    if(distancia < menorDistancia)
                        menorDistancia = distancia;
                }
            }
            System.out.println("Menor distancia para entrega: " + menorDistancia);
            
            if(menorDistancia == 999)//Nao foi possivel calcular rota para nenhuma das unidades
                return false;
            else
            {
                HttpSession sessao = req.getSession();
                sessao.setAttribute("endereco", endereco);
                return true;
            }
        }
    }  
    
    public boolean cadastrar(HttpServletRequest req, HttpServletResponse resp){
        Cliente cliente = (Cliente)req.getSession().getAttribute("cliente");
        
        String cep = req.getParameter("cep");
        String arrayCep[] = cep.split("-");
        cep = arrayCep[0] + arrayCep[1];
        
        Endereco endereco = new Endereco(0, cep);
        endereco.setEndereco(req.getParameter("endereco"));
        endereco.setNumero(Integer.parseInt(req.getParameter("numero")));
        endereco.setComplemento(req.getParameter("complemento"));
        endereco.setBairro(req.getParameter("bairro"));
        endereco.setCidade(req.getParameter("cidade"));
        endereco.setUf(req.getParameter("estado"));
        endereco.setCliente(cliente);
        
        int codendereco = enderecoDao.inserir(endereco);
        
        return (codendereco != 0);
    }
    
    public boolean alterar(HttpServletRequest req, HttpServletResponse resp){
        Cliente cliente = (Cliente)req.getSession().getAttribute("cliente");
 
        String cep = req.getParameter("cep");
        String arrayCep[] = cep.split("-");
        cep = arrayCep[0] + arrayCep[1];
        
        Endereco endereco = new Endereco(Integer.parseInt(req.getParameter("codendereco")), cep);
        endereco.setEndereco(req.getParameter("endereco"));
        endereco.setNumero(Integer.parseInt(req.getParameter("numero")));
        endereco.setComplemento(req.getParameter("complemento"));
        endereco.setBairro(req.getParameter("bairro"));
        endereco.setCidade(req.getParameter("cidade"));
        endereco.setUf(req.getParameter("estado"));
        endereco.setCliente(cliente);
        
        return enderecoDao.alterar(endereco);
    }
}
