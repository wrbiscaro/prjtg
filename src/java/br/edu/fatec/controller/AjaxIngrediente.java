/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Loja;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class AjaxIngrediente extends ClasseAbstrata {
    EstoqueDAO estoqueDao = null;
    LojaDAO lojaDao = null;
    
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        Conexao conexao = new Conexao();
        IngredienteDAO ingredienteDao = new IngredienteDAO(conexao.conectar());
        HttpSession sessao = req.getSession();
        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("excluir")){
                estoqueDao = new EstoqueDAO(conexao.conectar());
                lojaDao = new LojaDAO(conexao.conectar());        
                ArrayList<Loja> lojas = lojaDao.listar();
                
                int codingrediente = Integer.parseInt(req.getParameter("codingrediente"));
                
                boolean delete = ingredienteDao.excluir(codingrediente);
                
                if(delete) {
                    for(Loja l : lojas) {
                        estoqueDao.excluir(l.getCod(), codingrediente);
                    } 
                    
                    out.println("Ingrediente excluido com sucesso!");
                }
                else
                    out.println("Não foi possível excluir o ingrediente!");  
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }
    }    
}
