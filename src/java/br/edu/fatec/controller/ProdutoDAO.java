/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Ingrediente;
import br.edu.fatec.model.Produto;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import org.apache.commons.fileupload.FileItem;

/**
 *
 * @author luizdagoberto
 */
public class ProdutoDAO {
    private Connection conn;

    public ProdutoDAO(Connection conn) {
        this.conn = conn;
    }
    
    public Produto selecionar(int codproduto){
        Produto p = null;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbproduto WHERE cod_produto = ?");
            ps.setInt(1, codproduto);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                ArrayList<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
                
                p = new Produto(rs.getInt("cod_produto"), rs.getString("descricao_produto"));
                p.setPreco(rs.getBigDecimal("preco_produto"));
                p.setUnidade(rs.getString("unidade_produto"));
                p.setCategoria(rs.getString("categoria_produto"));
                
                try {
                    ps = conn.prepareStatement("SELECT i.*\n" +
                                               "FROM tbitem_produto ip\n" +
                                               "INNER JOIN tbingrediente i ON ip.cod_ingrediente = i.cod_ingrediente\n" +
                                               "WHERE ip.cod_produto = ?");
                    ps.setInt(1, p.getCod());

                    ResultSet rs2 = ps.executeQuery();
                    while(rs2.next()){              
                        Ingrediente i = new Ingrediente(rs2.getInt("cod_ingrediente"), rs2.getString("descricao_ingrediente"));
                        i.setUnidade(rs2.getString("unidade_ingrediente"));
                        i.setQtde_padrao(rs2.getDouble("qtde_padrao"));
                        ingredientes.add(i);
                    }
                    p.setIngrediente(ingredientes);
                } catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return p;
    }
    
    public ArrayList<Produto> listar(){
        ArrayList<Produto> produtos = new ArrayList<Produto>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbproduto");
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                ArrayList<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
                
                Produto p = new Produto(rs.getInt("cod_produto"), rs.getString("descricao_produto"));
                p.setPreco(rs.getBigDecimal("preco_produto"));
                p.setUnidade(rs.getString("unidade_produto"));
                p.setCategoria(rs.getString("categoria_produto"));
                
                try {
                    ps = conn.prepareStatement("SELECT i.*\n" +
                                               "FROM tbitem_produto ip\n" +
                                               "INNER JOIN tbingrediente i ON ip.cod_ingrediente = i.cod_ingrediente\n" +
                                               "WHERE ip.cod_produto = ?");
                    ps.setInt(1, p.getCod());

                    ResultSet rs2 = ps.executeQuery();
                    while(rs2.next()){              
                        Ingrediente i = new Ingrediente(rs2.getInt("cod_ingrediente"), rs2.getString("descricao_ingrediente"));
                        i.setUnidade(rs2.getString("unidade_ingrediente"));
                        i.setQtde_padrao(rs2.getDouble("qtde_padrao"));
                        ingredientes.add(i);
                    }
                    p.setIngrediente(ingredientes);
                    produtos.add(p);
                } catch (SQLException ex) {
                    System.out.println(ex.toString());
                }
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return produtos;
    }
    
    public int inserir(Produto p){
        int codproduto = 0;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbproduto (descricao_produto, preco_produto, unidade_produto, categoria_produto) VALUES(?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, p.getDescricao());
            ps.setBigDecimal(2, p.getPreco());
            ps.setString(3, p.getUnidade());
            ps.setString(4, p.getCategoria());
            
            ps.execute();

            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next())
                codproduto = rs.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return codproduto;     
    }
    
    public boolean alterar(Produto p){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbproduto SET descricao_produto = ?, preco_produto = ?, unidade_produto = ?, categoria_produto = ? WHERE cod_produto = ?");
            ps.setString(1, p.getDescricao());
            ps.setBigDecimal(2, p.getPreco());
            ps.setString(3, p.getUnidade());
            ps.setString(4, p.getCategoria());
            ps.setInt(5, p.getCod());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }        
        
        return true;    
    }
    
    public void excluir(int codproduto){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbproduto WHERE cod_produto = ?");
            ps.setInt(1, codproduto);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
    
    //Salva a foto
    public void inserirImagem(int codproduto, FileItem item) {
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement("UPDATE tbproduto SET foto = ? WHERE cod_produto = ?");
            ps.setBinaryStream(1, item.getInputStream(), (int) item.getSize());
            ps.setInt(2, codproduto);
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    //Recupera a foto e retorna como ImageIcon
    /*public ImageIcon recuperaImagem(int codproduto) {
        ImageIcon imagem = null;
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement("SELECT foto FROM tbproduto WHERE cod_produto = ?");
            ps.setInt(1, codproduto);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()) {
                Blob blob = (Blob) rs.getBlob("foto");

                if(blob != null)
                    imagem = new ImageIcon(blob.getBytes(1, (int) blob.length()));
            }
        } catch(SQLException ex) {
            ex.toString();
        } catch (Exception ex) {
            ex.toString();
        }

        return imagem;
    }*/
    
    //Recupera a foto e retorna como InputStream
    public InputStream recuperaImagem(int codproduto) {
        InputStream imagem = null;
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement("SELECT foto FROM tbproduto WHERE cod_produto = ?");
            ps.setInt(1, codproduto);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.next()) {
                imagem = rs.getBinaryStream("foto");
            }
        } catch(SQLException ex) {
            ex.toString();
        } catch (Exception ex) {
            ex.toString();
        }

        return imagem;
    }
}
