/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Estoque;
import br.edu.fatec.model.Ingrediente;
import br.edu.fatec.model.Loja;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class EstoqueDAO {
    private Connection conn;

    public EstoqueDAO(Connection conn) {
        this.conn = conn;
    }
    
    public ArrayList<Estoque> listarPorLoja(int codloja){
        ArrayList<Estoque> estoques = new ArrayList<Estoque>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT e.*, i.descricao_ingrediente, l.cep_loja\n" +
                                        "FROM tbestoque e\n" +
                                        "INNER JOIN tbingrediente i ON e.cod_ingrediente = i.cod_ingrediente\n" +
                                        "INNER JOIN tbloja l ON e.cod_loja = l.cod_loja\n" +
                                        "WHERE e.cod_loja = ?");
            ps.setInt(1, codloja);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Ingrediente i = new Ingrediente(rs.getInt("cod_ingrediente"), rs.getString("descricao_ingrediente"));
                Loja l = new Loja(rs.getInt("cod_loja"), rs.getString("cep_loja"));
                Estoque e = new Estoque();
                e.setIngrediente(i);
                e.setLoja(l);
                e.setEstoque_disponivel(rs.getDouble("estoque_disponivel"));
                e.setEstoque_minimo(rs.getDouble("estoque_minimo"));
                
                estoques.add(e);
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return estoques;
    }  
    
    public boolean atualizar(int codloja, int codingrediente, double qtde, int tipo){
        PreparedStatement ps = null;
        
        try {
            if(tipo <= 5) //Entrada no estoque
                ps = conn.prepareStatement("UPDATE tbestoque SET estoque_disponivel = estoque_disponivel + ? WHERE cod_loja = ? AND cod_ingrediente = ?");
            else //Saida do estoque
                ps = conn.prepareStatement("UPDATE tbestoque SET estoque_disponivel = estoque_disponivel - ? WHERE cod_loja = ? AND cod_ingrediente = ?");
                
            ps.setDouble(1, qtde);
            ps.setInt(2, codloja);
            ps.setInt(3, codingrediente);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }  
        return true;
    }
    
    public void inserirMovimentacao(int codloja, int codingrediente, double qtde, int codusuario, int tipomovimento){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbmovimentacao (cod_loja, cod_ingrediente, qtde, cod_usuario, tipomovimento, data) VALUES(?,?,?,?,?,NOW())");
            ps.setInt(1, codloja);
            ps.setInt(2, codingrediente);
            ps.setDouble(3, qtde);
            ps.setInt(4, codusuario);
            ps.setInt(5, tipomovimento);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }           
    }
    
    public Estoque selecionar(int codloja, int codingrediente){
        Estoque estoque = null;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT e.*, i.descricao_ingrediente, i.unidade_ingrediente, l.cep_loja, l.endereco_loja\n" +
                                        "FROM tbestoque e\n" +
                                        "INNER JOIN tbingrediente i ON e.cod_ingrediente = i.cod_ingrediente\n" +
                                        "INNER JOIN tbloja l ON e.cod_loja = l.cod_loja\n" +
                                        "WHERE e.cod_loja = ? AND e.cod_ingrediente = ?");
            ps.setInt(1, codloja);
            ps.setInt(2, codingrediente);
            
            ResultSet rs = ps.executeQuery();
            
            if(rs.first()){
                Ingrediente i = new Ingrediente(rs.getInt("cod_ingrediente"), rs.getString("descricao_ingrediente"));
                i.setUnidade(rs.getString("unidade_ingrediente"));
                Loja l = new Loja(rs.getInt("cod_loja"), rs.getString("cep_loja"));
                l.setEndereco(rs.getString("endereco_loja"));
                estoque = new Estoque();
                estoque.setIngrediente(i);
                estoque.setLoja(l);
                estoque.setEstoque_disponivel(rs.getDouble("estoque_disponivel"));
                estoque.setEstoque_minimo(rs.getDouble("estoque_minimo"));
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return estoque;
    } 
    
    public void inserir(int codloja, int codingrediente, double estoque_minimo){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbestoque (cod_loja, cod_ingrediente, estoque_disponivel, estoque_minimo) VALUES(?,?,?,?)");
            ps.setInt(1, codloja);
            ps.setInt(2, codingrediente);
            ps.setDouble(3, 0);
            ps.setDouble(4, estoque_minimo);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }                  
    }
    
    public void excluir(int codloja, int codingrediente){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbestoque WHERE cod_loja = ? AND cod_ingrediente = ?");
            ps.setInt(1, codloja);
            ps.setInt(2, codingrediente);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }                  
    }
    
    public boolean atualizarQuantidadeMinima(int codloja, int codingrediente, double qtde){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbestoque SET estoque_minimo = ? WHERE cod_loja = ? AND cod_ingrediente = ?");
                
            ps.setDouble(1, qtde);
            ps.setInt(2, codloja);
            ps.setInt(3, codingrediente);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }  
        return true;
    }
    
    public void excluirPorLoja(int codloja){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbestoque WHERE cod_loja = ?");
            ps.setInt(1, codloja);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }                  
    }
}
