/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Cliente;
import br.edu.fatec.model.Funcionario;
import br.edu.fatec.model.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class LogicaUsuario extends ClasseAbstrata {
    Conexao conexao = null;
    UsuarioDAO usuarioDao = null;
    Usuario usuario = null;
    ClienteDAO clienteDao = null;
    Cliente cliente = null;
    FuncionarioDAO funcionarioDao = null;
    Funcionario funcionario = null;
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        RequestDispatcher rd = null;
        conexao = new Conexao();
        usuarioDao = new UsuarioDAO(conexao.conectar());

        String opcao = req.getParameter("Usuario");
        
        if(opcao.equals("Logar")){ 
            this.logar(req, resp);
            
            HttpSession session = req.getSession();
            usuario = (Usuario)session.getAttribute("usuario");
            
            if(usuario == null)
            {
                req.setAttribute("msg", "Cadastro não encontrado! Cadastre-se ou informe dados válidos.");
                rd = req.getRequestDispatcher("login.jsp");
            }
            else{
                String tipousuario = usuario.getTipoUsuario();

                if(tipousuario.equals("C")){
                    clienteDao = new ClienteDAO(conexao.conectar());
                    cliente = clienteDao.selecionarPorUsuario(usuario.getCod());
                    cliente.setUsuario(usuario);
                    session.setAttribute("cliente", cliente);
                }
                else{
                    funcionarioDao = new FuncionarioDAO(conexao.conectar());
                    funcionario = funcionarioDao.selecionarPorUsuario(usuario.getCod());
                    funcionario.setUsuario(usuario);
                    session.setAttribute("funcionario", funcionario);               
                }
                conexao.fecharConexao();
                //Redirecionar para a pagina de enderecos
                rd = req.getRequestDispatcher("index.jsp");
                
                //req.setAttribute("msg", "Entrou!");
                //rd = req.getRequestDispatcher("index.jsp");
            }
        }
        
        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            System.out.println(ex.toString());
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
    }
   
        public void logar(HttpServletRequest req, HttpServletResponse resp){
            String e = req.getParameter("email");
            String s = req.getParameter("senha");
            
            usuario = usuarioDao.logar(e, s);
            
            HttpSession session = req.getSession();
            session.setAttribute("usuario", usuario);
        }
    
}
