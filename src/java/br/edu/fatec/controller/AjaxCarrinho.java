/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.ItemPedido;
import br.edu.fatec.model.Pedido;
import br.edu.fatec.model.Produto;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class AjaxCarrinho extends ClasseAbstrata{
    private Conexao Conexao;

    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession sessao = req.getSession();
        Conexao conexao = new Conexao();
        ProdutoDAO produtodao = new ProdutoDAO(conexao.conectar());
        Pedido pedido = (Pedido)sessao.getAttribute("pedido");
        
        int qtdeProdutos = 0;
        String carrinho = null;
        
        try {
            PrintWriter out = resp.getWriter();
            String acao = req.getParameter("acao");
            int codproduto = Integer.parseInt(req.getParameter("codproduto"));
            int qtde = Integer.parseInt(req.getParameter("qtde"));
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8"); 
            
            if(acao.equals("carregar")){
                if((pedido != null) && (pedido.getItempedido() != null))
                    qtdeProdutos = pedido.getItempedido().size();
            }
            else if(acao.equals("remover")){
                int indice = pedido.pesquisaProduto(codproduto);

                //Retira o valor do item pedido do carrinho antes de excui-lo, para inserir no fim
                pedido.setValor_total(pedido.getValor_total().subtract(pedido.getItempedido().get(indice).getValor()));
                    
                //Tira o itempedido do carrinho e adiciona novamente no fim, atualizado
                pedido.getItempedido().remove(indice);
            }
            else{
                Produto p = produtodao.selecionar(codproduto);
                
                if(pedido == null)
                {
                    pedido = new Pedido(0);
                    pedido.setValor_total(BigDecimal.ZERO);
                }
                
                //Cria um novo item pedido. Se nao tiver esse produto no pedido, ele é adicionado. 
                //se tiver, o mesmo é removido e é adicionado este no lugar.
                ItemPedido ip = new ItemPedido();
                ip.setPedido(pedido.getCod());
                ip.setProduto(p);
                    
                //se já possui o produto no pedido, pega a quantidade existente, soma na quantidade recebida 
                //do cardapio e exclui o produto do pedido, para adicionar no fim com a quantidade atualizada
                if(pedido.pesquisaProduto(p.getCod()) != null){
                    int indice = pedido.pesquisaProduto(p.getCod());
                    
                    /*Se a acao nao for atualizar, pega o quantidade que esta no carrinho e adiciona na quantidade recebida do cardapio.
                      Se for atalizar, apenas pega a qtde recebida e substitui no fim*/
                    if(!acao.equals("atualizar")){                       
                        qtde = qtde + pedido.getItempedido().get(indice).getQtde();                    
                    }
    
                    //Retira o valor do item pedido do carrinho antes de excui-lo, para inserir no fim
                    pedido.setValor_total(pedido.getValor_total().subtract(pedido.getItempedido().get(indice).getValor()));
                    
                    //Tira o itempedido do carrinho e adiciona novamente no fim, atualizado
                    pedido.getItempedido().remove(indice);
                }
                ip.setQtde(qtde);
                    
                //Calcula o valor do item pedido, de acordo com o preco no momento da compra
                BigDecimal valor = p.getPreco().multiply(new BigDecimal(String.valueOf(qtde)));
                //System.out.println(valor);
                ip.setValor(valor);
                pedido.getItempedido().add(ip);
                pedido.setValor_total(pedido.getValor_total().add(valor));
                //System.out.println(pedido.getValor_total());
                
                sessao.setAttribute("pedido", pedido);
                qtdeProdutos = pedido.getItempedido().size();
            } 
            
            if(acao.equals("atualizar")){
                String total = "<h3 class=\"text-right\">Total = <b>R$</b> <strong>" + pedido.getValor_total() + "</strong></h3>\n";
                out.println(total);
            }
            else{
                carrinho = "<a class=\"navbar-brand\" href=\"http://localhost:8084/prjTG/carrinho.jsp\"><span style=\"margin-right:5px;\" class=\"glyphicon glyphicon-shopping-cart\"></span>Carrinho (" + qtdeProdutos + " itens)</a>";
                out.println(carrinho);           
                //response.getWriter().write(dados); 
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }
    }
    
}
