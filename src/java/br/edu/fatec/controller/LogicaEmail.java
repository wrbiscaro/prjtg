/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Estoque;
import br.edu.fatec.model.Ingrediente;
import br.edu.fatec.model.Loja;
import br.edu.fatec.model.MailJava;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author luizdagoberto
 */
public class LogicaEmail extends ClasseAbstrata {

    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        RequestDispatcher rd = null;
        String opcao = req.getParameter("Email");
        
        if(opcao.equals("Enviar")){
            this.enviarEmail(req, resp);
            req.setAttribute("msg", "Email enviado com sucesso!");
            rd = req.getRequestDispatcher("teste_email.jsp");     
        }
        
        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(LogicaCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogicaCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void enviarEmail(HttpServletRequest req, HttpServletResponse resp){
        MailJava mj = new MailJava();
        
        //configuracoes de envio
        mj.setSmtpHostMail("smtp.gmail.com");
        mj.setSmtpPortMail("587");
        mj.setSmtpAuth("true");
        mj.setSmtpStarttls("true");
        mj.setUserMail("wrbiscaro@gmail.com");
        mj.setFromNameMail("Wallace Biscaro");
        mj.setPassMail("wr1071114");
        mj.setCharsetMail("ISO-8859-1"); //Necessario para email HTML
        mj.setSubjectMail("Teste de Envio de Email");
        mj.setBodyMail(htmlMessage()); //Ou mj.setBodyMail(textMessage()) p/ uma mensagem de texto
        mj.setTypeTextMail(MailJava.TYPE_TEXT_HTML); //Ou mj.setTypeTextMail(MailJava.TYPE_TEXT_PLAIN) p/ uma mensagem de texto
 
        //sete quantos destinatarios desejar
        Map<String, String> map = new HashMap<String, String>();
        map.put("wrbiscaro@hotmail.com", "Wallace Biscaro");
        /*
        map.put("destinatario2@msn.com", "email msn");
        map.put("destinatario3@ig.com.br", "email ig");
        */
 
        mj.setToMailsUsers(map);
 
        //seta quantos anexos desejar
        List<String> files = new ArrayList<String>();
        files.add("C:\\Users\\luizdagoberto\\Documents\\assinatura_wallace.gif");
        /*
        files.add("C:\images\hover_next.png");
        files.add("C:\images\hover_prev.png");
        */
 
        mj.setFileMails(files);
 
        try {
            new MailJavaSender().senderMail(mj);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
 
    public void enviarEmailCompra(Estoque estoque){
        MailJava mj = new MailJava();

        mj.setSmtpHostMail("smtp.gmail.com");
        mj.setSmtpPortMail("587");
        mj.setSmtpAuth("true");
        mj.setSmtpStarttls("true");
        mj.setUserMail("wrbiscaro@gmail.com");//Criar um email para o sistema
        mj.setFromNameMail("Wallace Biscaro");
        mj.setPassMail("wr1071114");//Senha do email do sistema
        mj.setCharsetMail("ISO-8859-1");
        mj.setSubjectMail("Pedido de Compra");
        mj.setBodyMail(mensagemCompra(estoque));
        mj.setTypeTextMail(MailJava.TYPE_TEXT_HTML);
        
        Map<String, String> map = new HashMap<String, String>();
        map.put("wrbiscaro@hotmail.com", "Wallace Biscaro");//Criar um email para o setor de compras ou fornecedor 
        mj.setToMailsUsers(map);
        
        List<String> files = new ArrayList<String>();
        mj.setFileMails(files);
 
        try {
            new MailJavaSender().senderMail(mj);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
    //Metodo que retorna uma mensagem de texto
    private static String textMessage() {
        return  "Teste de mensagem de texto.";
    }
 
    //Metodo que retorna uma mensagem em HTML
    private static String htmlMessage() {
        return
        "<html>" +
            "<head>" +
                "<title>Mensagem HTML</title>" +
            "</head>" +
            "<body>" +
                "<div style='width:28%; height:100px;'>" +
                    "<h3>Teste de mensagem HTML.</h3>" +
                    "<br/>" +
                    "<p>Visite o Google: <a href='http://www.google.com.br/'>www.google.com.br</a></p>" +
                "</div>" +
            "</body>" +
        "</html>";
    }
    
    private static String mensagemCompra(Estoque estoque){
        return
        "<html>" +
            "<head>" +
                "<title>Pedido de Compra</title>" +
            "</head>" +
            "<body>" +
                "<div>" +
                    "<h3>Pedido de Compra para a Loja " + estoque.getLoja().getCod() + " - " + estoque.getLoja().getEndereco() + "</h3>" +
                    "<br/>" +
                    "<p>A loja " + estoque.getLoja().getCod() + " - " + estoque.getLoja().getEndereco() + " está com apenas " + estoque.getEstoque_disponivel() +
                    " " + estoque.getIngrediente().getUnidade() + " de " + estoque.getIngrediente().getDescricao() + 
                    " no estoque, portanto abaixo do estoque mínimo (" + estoque.getEstoque_minimo() + " " + estoque.getIngrediente().getUnidade() + ").</p>" +
                    "<p>Entrar em contato com o fornecedor e efetuar um pedido de compra.</p>" +
                "</div>" +
            "</body>" +
        "</html>";
    }
}
