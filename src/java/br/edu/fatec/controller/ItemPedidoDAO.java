/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.ItemPedido;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author luizdagoberto
 */
public class ItemPedidoDAO {
    private Connection conn;

    public ItemPedidoDAO(Connection conn) {
        this.conn = conn;
    }
    
    public void inserir(ItemPedido ip){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbitem_pedido (cod_pedido, cod_produto, qtde_item_pedido, valor_item_pedido) VALUES(?,?,?,?)");
            ps.setInt(1, ip.getPedido());
            ps.setInt(2, ip.getProduto().getCod());
            ps.setInt(3, ip.getQtde());
            ps.setBigDecimal(4, ip.getValor());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }           
    }
}
