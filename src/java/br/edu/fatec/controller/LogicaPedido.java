/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Cliente;
import br.edu.fatec.model.Endereco;
import br.edu.fatec.model.Estoque;
import br.edu.fatec.model.Funcionario;
import br.edu.fatec.model.ItemPedido;
import br.edu.fatec.model.ItemProduto;
import br.edu.fatec.model.Loja;
import br.edu.fatec.model.Pedido;
import br.edu.fatec.model.Rota;
import br.edu.fatec.model.Usuario;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class LogicaPedido extends ClasseAbstrata {
    Conexao conexao = null;
    PedidoDAO pedidoDao = null;
    ItemPedidoDAO itemPedidoDao = null;
    EnderecoDAO enderecoDao = null;
    LojaDAO lojaDao = null;
    ItemProdutoDAO itemProdutoDao = null;
    EstoqueDAO estoqueDao = null;
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        conexao = new Conexao();
        RequestDispatcher rd = null;
        String opcao = req.getParameter("Pedido");
        String url = "";
        
        if(opcao.equals("Inserir")){
            boolean resultado = this.inserir(req, resp);
            
            if(resultado){
                //req.setAttribute("msg", "Pedido realizado com sucesso! Acompanhe seus pedidos abaixo.");                 
                //rd = req.getRequestDispatcher("cliente/meus_pedidos.jsp"); 
                url = "cliente/meus_pedidos.jsp?msg=sucesso";
            }               
            else{
                //req.setAttribute("msg", "Pedido não finalizado!");
                //rd = req.getRequestDispatcher("cliente/fecharpedido.jsp");  
                url = "carrinho.jsp?msg=erro";
            }
        
            try {
                resp.sendRedirect(url);
            }catch (IOException ex) {
                Logger.getLogger(ex.toString());
            }
        }
        else if(opcao.equals("GerarRelatorio")){
            int relatorio = Integer.parseInt(req.getParameter("relatorio"));//1: Por unidade, 2: Geral
            
            if(relatorio == 1){
                this.gerarRelatorio(req, resp);
                rd = req.getRequestDispatcher("gerente/exibir_relatorio.jsp");
            }
            else{
                this.gerarRelatorioGeral(req, resp);
                rd = req.getRequestDispatcher("gerente/exibir_relatorio_geral.jsp");            
            }
            
            try {
                rd.forward(req, resp);
            } catch (ServletException ex) {
                Logger.getLogger(ex.toString());
            } catch (IOException ex) {
                Logger.getLogger(ex.toString());
            }
        }
    }
    
    public boolean inserir(HttpServletRequest req, HttpServletResponse resp){
        pedidoDao = new PedidoDAO(conexao.conectar());
        itemPedidoDao = new ItemPedidoDAO(conexao.conectar());
        enderecoDao = new EnderecoDAO(conexao.conectar());
        lojaDao = new LojaDAO(conexao.conectar());
        itemProdutoDao = new ItemProdutoDAO(conexao.conectar());
        estoqueDao = new EstoqueDAO(conexao.conectar());
        Rota r = new Rota();
        HttpSession session = req.getSession();
        
        Cliente cliente = (Cliente)session.getAttribute("cliente");
        Usuario usuario = (Usuario)session.getAttribute("usuario");
        Pedido pedido = (Pedido)session.getAttribute("pedido");
        pedido.setObservacao(req.getParameter("observacoes"));
        pedido.setForma_pagamento(Integer.parseInt(req.getParameter("forma_pagamento")));
        pedido.setCliente(cliente);
        //Trata o troco
        String troco = req.getParameter("troco");
        if(troco.equals(""))
            pedido.setTroco(BigDecimal.ZERO);
        else
            pedido.setTroco(new BigDecimal(troco));
        
        int codendereco = Integer.parseInt(req.getParameter("codendereco"));
        Endereco e = enderecoDao.selecionar(codendereco);
        pedido.setEndereco(e);
        
        //Calcula a rota para a loja mais proxima ao endereco
        ArrayList<Loja> lojas = lojaDao.listar();
        double distancia, menorDistancia = 999;
        Loja lojaProxima = null;
        
        String strEndereco = e.getEndereco() + ", " + e.getCidade() + " - " + e.getUf();
        strEndereco = r.removerAcentos(strEndereco);        
 
        for(Loja u : lojas){
            String loja = u.getEndereco() + ", " + u.getCidade() + " - " + u.getUf();
            loja = r.removerAcentos(loja);
                
            distancia = r.calcular(loja, strEndereco);
                
            if(distancia == -1)
                System.out.println("Não foi possivel calcular a rota para a unidade " + u);
            else
            {
                System.out.println("Distancia para a unidade " + loja + ": " + distancia);
                    
                if(distancia < menorDistancia)
                {
                   menorDistancia = distancia;
                   lojaProxima = u;
                }
            }
        }
        System.out.println("Menor distancia para entrega: " + menorDistancia);
        //Armazena a loja mais proxima ao endereco do pedido
        pedido.setLoja(lojaProxima);
        
        //Verifica se todos os ingredientes do produto estao disponiveis em estoque.        
        ArrayList<ItemPedido> itempedido = pedido.getItempedido();
        for(ItemPedido ip : itempedido){
            ArrayList<ItemProduto> itemprodutos = itemProdutoDao.listarPorProduto(ip.getProduto().getCod());
            Map<Integer, Double> estoqueTemporario = new HashMap<Integer, Double>();//Armezana estoque temporario
            
            for(int cont = 0; cont < ip.getQtde(); cont++){
                for(ItemProduto itemproduto : itemprodutos){
                    Estoque estoque = null;
                    
                    //Se o ingrediente é armazenado no estoque, verifica se tem quantidade suficiente
                    if(itemproduto.getIngrediente().getEstoque().equals("S"))
                    {
                        if(cont == 0){
                            estoque = estoqueDao.selecionar(pedido.getLoja().getCod(), itemproduto.getIngrediente().getCod());
                            estoqueTemporario.put(itemproduto.getIngrediente().getCod(), estoque.getEstoque_disponivel());
                        }  
                        
                        if(itemproduto.getQtde() > estoqueTemporario.get(itemproduto.getIngrediente().getCod())){ //Nao tem o produto em estoque
                            req.setAttribute("msg", "Pedido não finalizado! Entre em contato pelo telefone " + pedido.getLoja().getTelefone() + " para maiores informções.");
                            return false;
                        } 
                        
                        double qtdeAtual = estoqueTemporario.get(itemproduto.getIngrediente().getCod()) - itemproduto.getQtde();
                        estoqueTemporario.put(itemproduto.getIngrediente().getCod(), qtdeAtual);
                        System.out.println(itemproduto.getIngrediente().getCod() + " - " + estoqueTemporario.get(itemproduto.getIngrediente().getCod()));
                    }
                }
            }
        }    
        
        //Insere o pedido
        int codpedido = pedidoDao.inserir(pedido);
        pedido.setCod(codpedido);
        
        //Retira os produtos do estoque
        
        //Para cada produto do pedido
        for(ItemPedido ip : itempedido){
            //Grava os itens do pedido na tbitem_pedido
            ip.setPedido(codpedido);
            itemPedidoDao.inserir(ip);
            
            //Para cada quantidade desse produto no pedido. Ex: 2 X-Bacon
            for(int cont = 0; cont < ip.getQtde(); cont++){
                //Lista todos ingredientes do produto
                ArrayList<ItemProduto> itemprodutos = itemProdutoDao.listarPorProduto(ip.getProduto().getCod());
                
                //Para cada ingrediente do produto, retira a quantidade utilizada do estoque
                for(ItemProduto itemproduto : itemprodutos)
                {
                    //Se o ingrediente é armazenado no estoque, retira a quantidade utilizada
                    if(itemproduto.getIngrediente().getEstoque().equals("S"))
                    {
                        Estoque estoque = estoqueDao.selecionar(pedido.getLoja().getCod(), itemproduto.getIngrediente().getCod());

                        estoqueDao.atualizar(pedido.getLoja().getCod(), itemproduto.getIngrediente().getCod(), itemproduto.getQtde(), 6);
                        estoqueDao.inserirMovimentacao(pedido.getLoja().getCod(), itemproduto.getIngrediente().getCod(), itemproduto.getQtde(), usuario.getCod(), 6);
                        
                        //Se o estoque disponivel ficou ficar abaixo do estoque minimo, envia email
                        if((estoque.getEstoque_disponivel() - itemproduto.getQtde()) < estoque.getEstoque_minimo()){
                            LogicaEmail le = new LogicaEmail();

                            estoque.setEstoque_disponivel((estoque.getEstoque_disponivel() - itemproduto.getQtde()));
                            le.enviarEmailCompra(estoque); //Envia o email de aviso para o setor de compra ou fornecedor
                        } 
                    }
                }
            }
        } 
        
        session.removeAttribute("pedido");
        return true;
    }
    
    public void gerarRelatorio(HttpServletRequest req, HttpServletResponse resp){
        pedidoDao = new PedidoDAO(conexao.conectar());
        HttpSession session = req.getSession();
        Funcionario funcionario = (Funcionario)session.getAttribute("funcionario");
        String arrayData[];
        
        String data_inicial = req.getParameter("data_inicial");
        arrayData = data_inicial.split("/");
        data_inicial = arrayData[2] + "-" + arrayData[1] + "-" + arrayData[0];
        
        String data_final = req.getParameter("data_final");
        arrayData = data_final.split("/");
        data_final = arrayData[2] + "-" + arrayData[1] + "-" + arrayData[0];
        
        ArrayList<Pedido> pedidos = pedidoDao.relatorioPorUnidade(data_inicial, data_final, funcionario.getLoja().getCod());

        if(pedidos.isEmpty()){
            pedidos = null;
            req.setAttribute("msg", "Nenhum pedido para o período selecionado!");
        }
        else{
            BigDecimal totalperiodo = BigDecimal.ZERO;
                
            for(Pedido p : pedidos){
                totalperiodo = totalperiodo.add(p.getValor_total());
            }

            req.setAttribute("totalperiodo", totalperiodo);
        }

        req.setAttribute("pedidos", pedidos);       
        req.setAttribute("datainicial", req.getParameter("data_inicial"));
        req.setAttribute("datafinal", req.getParameter("data_final"));
    }
    
    public void gerarRelatorioGeral(HttpServletRequest req, HttpServletResponse resp){
        lojaDao = new LojaDAO(conexao.conectar());
        pedidoDao = new PedidoDAO(conexao.conectar());
        String arrayData[];
        String strLojas = "";
        String strValores = "";
                   
        ArrayList<Loja> lojas = lojaDao.listar();
        
        String data_inicial = req.getParameter("data_inicial");
        arrayData = data_inicial.split("/");
        data_inicial = arrayData[2] + "-" + arrayData[1] + "-" + arrayData[0];
        
        String data_final = req.getParameter("data_final");
        arrayData = data_final.split("/");
        data_final = arrayData[2] + "-" + arrayData[1] + "-" + arrayData[0];
    
        
        for(Loja l : lojas){
            BigDecimal valor = pedidoDao.relatorioTotalPorUnidade(data_inicial, data_final, l.getCod());
            
            strLojas += l.getEndereco() + "|";
            strValores += String.valueOf(valor) + "|";
        }
        
        strLojas = strLojas.substring(0, (strLojas.length() - 1));
        strValores = strValores.substring(0, (strValores.length() - 1));      
        
        req.setAttribute("lojas", strLojas);
        req.setAttribute("valores", strValores);
        req.setAttribute("datainicial", req.getParameter("data_inicial"));
        req.setAttribute("datafinal", req.getParameter("data_final"));
    }
}
