/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Estoque;
import br.edu.fatec.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class AjaxEstoque extends ClasseAbstrata{
    private Conexao Conexao;
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        Conexao conexao = new Conexao();
        EstoqueDAO estoqueDao = new EstoqueDAO(conexao.conectar());
        HttpSession sessao = req.getSession();
        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("atualizarEstoque")){
                int codloja = Integer.parseInt(req.getParameter("codloja"));
                int codingrediente = Integer.parseInt(req.getParameter("codingrediente"));
                int tipomovimento = Integer.parseInt(req.getParameter("tipomovimento"));
                double qtde = Double.parseDouble(req.getParameter("qtde"));
                boolean update;
                
                Estoque estoque = estoqueDao.selecionar(codloja, codingrediente);
                
                if(tipomovimento > 5){ //Saida do estoque
                    if(qtde > estoque.getEstoque_disponivel()){
                        update = false;
                        out.println("0"); //Mostra mensagem de quantidade insuficiente em estoque
                    }
                    else{
                        update = estoqueDao.atualizar(codloja, codingrediente, qtde, tipomovimento);
                        
                        if((estoque.getEstoque_disponivel() - qtde) < estoque.getEstoque_minimo()){
                            LogicaEmail le = new LogicaEmail();
                            
                            estoque.setEstoque_disponivel((estoque.getEstoque_disponivel() - qtde));
                            le.enviarEmailCompra(estoque); //Envia o email de aviso para o setor de compra ou fornecedor
                        }
                    }
                }
                else
                    update = estoqueDao.atualizar(codloja, codingrediente, qtde, tipomovimento);
                
                if(update){
                    Usuario usuario = (Usuario)sessao.getAttribute("usuario");
                    estoqueDao.inserirMovimentacao(codloja, codingrediente, qtde, usuario.getCod(), tipomovimento);
                    out.println("1"); //Recarrega a pagina para exibir a nova quantidade
                }
                      
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }        
    }
    
}
