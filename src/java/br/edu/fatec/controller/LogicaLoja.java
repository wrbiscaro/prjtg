/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fatec.controller;

import br.edu.fatec.model.Estoque;
import br.edu.fatec.model.Ingrediente;
import br.edu.fatec.model.Loja;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wallace Biscaro
 */
public class LogicaLoja extends ClasseAbstrata {
    Conexao conexao = null;
    LojaDAO lojaDao = null;
    IngredienteDAO ingredienteDao = null;
    EstoqueDAO estoqueDao = null;
    
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        RequestDispatcher rd = null;
        conexao = new Conexao();
        lojaDao = new LojaDAO(conexao.conectar());
        ingredienteDao = new IngredienteDAO(conexao.conectar());
        estoqueDao = new EstoqueDAO(conexao.conectar());
        
        String opcao = req.getParameter("Loja");
        
        if(opcao.equals("Cadastrar")){
            boolean cadastrado = this.cadastrar(req, resp);
            
            if(cadastrado)
                req.setAttribute("msg", "Loja cadastrada com sucesso!");
            else
                req.setAttribute("msg", "Erro ao cadastrar a loja! Verifique os dados informados.");           

            rd = req.getRequestDispatcher("gerente/cadastrar_unidade.jsp"); 
        }
        else if(opcao.equals("Alterar")){
            boolean alterado = this.alterar(req, resp);
            
            if(alterado)
                req.setAttribute("msg", "Loja alterada com sucesso!");
            else
                req.setAttribute("msg", "Erro ao alterar a loja! Verifique os dados informados.");           

            rd = req.getRequestDispatcher("gerente/alterar_unidade.jsp"); 
        }
        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(LogicaEndereco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogicaEndereco.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    public boolean cadastrar(HttpServletRequest req, HttpServletResponse resp){
        String cep = req.getParameter("cep");
        String arrayCep[] = cep.split("-");
        cep = arrayCep[0] + arrayCep[1];
        
        Loja loja = new Loja(0, cep);
        loja.setEndereco(req.getParameter("endereco"));
        loja.setNumero(Integer.parseInt(req.getParameter("numero")));
        loja.setComplemento(req.getParameter("complemento"));
        loja.setBairro(req.getParameter("bairro"));
        loja.setCidade(req.getParameter("cidade"));
        loja.setUf(req.getParameter("estado"));
        loja.setEmail(req.getParameter("email"));
        loja.setTelefone(req.getParameter("telefone"));
        
        int codloja = lojaDao.inserir(loja);
        
        //Insere registros para todos os ingredientes na tbestoque
        ArrayList<Ingrediente> ingredientes = ingredienteDao.listar();
        
        for(Ingrediente i : ingredientes)
            if(i.getEstoque().equals("S")){
                Estoque e = estoqueDao.selecionar(1, i.getCod());
                estoqueDao.inserir(codloja, i.getCod(), e.getEstoque_minimo());
            }
        
        return (codloja != 0);
    }
    
    public boolean alterar(HttpServletRequest req, HttpServletResponse resp){
        String cep = req.getParameter("cep");
        String arrayCep[] = cep.split("-");
        cep = arrayCep[0] + arrayCep[1];
        
        Loja loja = new Loja(Integer.parseInt(req.getParameter("codloja")), cep);
        loja.setEndereco(req.getParameter("endereco"));
        loja.setNumero(Integer.parseInt(req.getParameter("numero")));
        loja.setComplemento(req.getParameter("complemento"));
        loja.setBairro(req.getParameter("bairro"));
        loja.setCidade(req.getParameter("cidade"));
        loja.setUf(req.getParameter("estado"));
        loja.setTelefone(req.getParameter("telefone"));
        loja.setEmail(req.getParameter("email"));

        
        return lojaDao.alterar(loja);
    }   
}
