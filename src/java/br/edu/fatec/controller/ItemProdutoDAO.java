/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Ingrediente;
import br.edu.fatec.model.ItemProduto;
import br.edu.fatec.model.Produto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luizdagoberto
 */
public class ItemProdutoDAO {
    private Connection conn;

    public ItemProdutoDAO(Connection conn) {
        this.conn = conn;
    }
    
   public ArrayList<ItemProduto> listarPorProduto(int codproduto){
       ArrayList<ItemProduto> itemprodutos = new ArrayList<ItemProduto>();
       PreparedStatement ps = null;
       
        try {
            ps = conn.prepareStatement("SELECT p.cod_produto, p.descricao_produto, i.cod_ingrediente, i.descricao_ingrediente, i.estoque, ip.qtde_item_produto\n" +
                                        "FROM tbitem_produto ip\n" +
                                        "INNER JOIN tbproduto p ON ip.cod_produto = p.cod_produto\n" +
                                        "INNER JOIN tbingrediente i ON ip.cod_ingrediente = i.cod_ingrediente\n" +
                                        "WHERE p.cod_produto = ?");
            ps.setInt(1, codproduto);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Produto p = new Produto(rs.getInt("cod_produto"), rs.getString("descricao_produto"));
                Ingrediente i = new Ingrediente(rs.getInt("cod_ingrediente"), rs.getString("descricao_ingrediente"));
                i.setEstoque(rs.getString("estoque"));
                
                ItemProduto ip = new ItemProduto();
                ip.setProduto(p);
                ip.setIngrediente(i);
                ip.setQtde(rs.getDouble("qtde_item_produto"));
                
                itemprodutos.add(ip);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ex.toString());
        }  
        return itemprodutos;
   } 
   
   public void inserir(ItemProduto ip){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbitem_produto (cod_produto, cod_ingrediente, qtde_item_produto) VALUES(?,?,?)");
            ps.setInt(1, ip.getProduto().getCod());
            ps.setInt(2, ip.getIngrediente().getCod());
            ps.setDouble(3, ip.getQtde());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }         
    }
   
    public void excluirPorProduto(int codproduto){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbitem_produto WHERE cod_produto = ?");
            ps.setInt(1, codproduto);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
}
