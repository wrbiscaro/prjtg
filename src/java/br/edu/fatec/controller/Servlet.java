/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.controller.Conexao;
import br.edu.fatec.model.Ingrediente;
import br.edu.fatec.model.ItemProduto;
import br.edu.fatec.model.Produto;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.swing.JOptionPane;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
/*import br.edu.fatec.control.DaoLogin;
import br.edu.fatec.model.Login;*/
/**
 *
 * @author luizdagoberto
 */
public class Servlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    //Declaracao das variaveis
    private Conexao conexao = null;
    /*private DaoLogin daoLogin = null;
    private Login login = null;*/
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Servlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Servlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Verificar parametros que fazem a requisicao
        /*Enumeration paramaterNames = request.getParameterNames();
        while(paramaterNames.hasMoreElements() ) {
            System.out.println(paramaterNames.nextElement());
        }*/
        
        //O metodo get e utilizado para recuperar imagens do banco
        Conexao conexao = new Conexao();
        InputStream leitura = null; //Recebe sempre o InputStream da classe DAO referente a classe recebida pela requisicao 

        String cod = request.getParameter("cod");
        String classe = request.getParameter("classe");
        
        if(classe.equals("Produto")){
            ProdutoDAO produtoDao = new ProdutoDAO(conexao.conectar());  
            leitura = produtoDao.recuperaImagem(Integer.parseInt(cod));
        }

        ByteArrayOutputStream buffer = new ByteArrayOutputStream(1024);  
        
        //Se recebeu a imagem da DAO correspondente a requisicao
        if(leitura != null) {
            int lido = 0;
            
            lido = leitura.read();  
            while (lido != -1) {  
                buffer.write(lido);  
                lido = leitura.read();  
            }  

            //Converte buffer em array de bytes e envia para a jsp que fez a requisicao  
            response.getOutputStream().write(buffer.toByteArray());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String parametroLogica = "";
        
        boolean isMultiPart = FileUpload.isMultipartContent(request);
        System.out.println("isMultiPart: " + isMultiPart);
        
        /*
        Se eh uma requisicao multipart, utiliza o codigo abaixo (dentro do if) para verificar o nome da classe e redirecionar.
        Nesse tipo de requisicao o request.getParameter() nao funciona.
        Requisicao multipart e utilizada para envio de fotos, arquivos, etc. 
        A logica que vai receber esta requisicao deve trata-la igual ao codigo abaixo (dentro do if).
        */
        if(isMultiPart){    
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                List items = upload.parseRequest(request);
                Iterator iter = items.iterator();
                
                //Logica do Produto (Colocar o codigo abaixo na classe LogicaProduto)
                Conexao conexao = new Conexao();
                ProdutoDAO produtoDao = new ProdutoDAO(conexao.conectar());
                ItemProdutoDAO itemProdutoDao = new ItemProdutoDAO(conexao.conectar());
                Produto produto = new Produto(0, "");
                FileItem foto = null;
                ArrayList<String> ingredientes = new ArrayList<String>();

                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();
                    
                    if (item.getFieldName().equals("codproduto")) {
                        produto.setCod(Integer.parseInt(item.getString()));
                    }
                    
                    if (item.getFieldName().equals("descricao")) {
                        produto.setDescricao(item.getString());
                    }
                    
                    if (item.getFieldName().equals("preco")) {
                        produto.setPreco(new BigDecimal(item.getString()));
                    }
                    
                    if (item.getFieldName().equals("unidade")) {
                        produto.setUnidade(item.getString());
                    }
                    
                    if (item.getFieldName().equals("categoria")) {
                        produto.setCategoria(item.getString());
                    }
                    
                    if (item.getFieldName().equals("chk_ingrediente")) {
                        ingredientes.add(item.getString());
                    }
                    
                    //Adiciona a foto
                    if (!item.isFormField()) {
                        if (item.getName().length() > 0) {
                            foto = item;    
                        }
                    }
                }
                
                if(produto.getCod() != 0){                    
                    boolean alterado = produtoDao.alterar(produto);
                    
                    if(foto != null)
                        produtoDao.inserirImagem(produto.getCod(), foto);
                    
                    if(alterado)
                        request.setAttribute("msg", "Produto alterado com sucesso!");
                    else
                        request.setAttribute("msg", "Erro ao alterar produto!");

                    RequestDispatcher rd = request.getRequestDispatcher("funcionario/alterar_produto.jsp?codproduto=" + produto.getCod());  
                    rd.forward(request, response);
                }
            }
            catch (FileUploadException ex) {
                ex.toString();
            }
            catch (Exception ex) { 
                ex.toString();
            }
        }
        else { //Requisicao comum
            parametroLogica = request.getParameter("classe");
            
            //MVC
            String pacote = "br.edu.fatec.controller.";
            Class logica = null;

            String nomeClasse = pacote + parametroLogica;
            try {             
                logica = Class.forName(nomeClasse);

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            ClasseAbstrata al = null;

            try {  
                al = (ClasseAbstrata)logica.newInstance();

                al.executa(request, response);
            } catch (InstantiationException ex) {
                Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
