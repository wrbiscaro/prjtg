/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fatec.controller;

import br.edu.fatec.model.Ingrediente;
import br.edu.fatec.model.ItemProduto;
import br.edu.fatec.model.Produto;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//Imports para a manipulacao de imagem
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;


/**
 *
 * @author Wallace Biscaro
 */
public class LogicaProduto extends ClasseAbstrata{
    Conexao conexao = null;
    ProdutoDAO produtoDao = null;
    ItemProdutoDAO itemProdutoDao = null;
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        RequestDispatcher rd = null;
        conexao = new Conexao();
        produtoDao = new ProdutoDAO(conexao.conectar());
      
        /*if(isMultiPart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            FileItem foto = null;

            try {
                List items = upload.parseRequest(req);
                Iterator iter = items.iterator();

                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                    if (item.getFieldName().equals("Produto")) {
                        opcao = item.getString();
                    }

                    if (!item.isFormField()) {
                        if (item.getName().length() > 0) {
                            System.out.println("Foto: " + item);
                            foto = item;    
                        }
                    }
                }
                
                if(foto != null)
                    produtoDao.inserirImagem(foto);
            }
            catch(FileUploadException ex) {
                ex.toString();
            }
            catch (Exception ex) { 
                ex.toString();
            }
        }
        else
            opcao = req.getParameter("Produto");   */        
        String opcao = req.getParameter("Produto");

        if(opcao.equals("Cadastrar")){
            this.cadastrar(req, resp);
            req.setAttribute("msg", "Produto cadastrado com sucesso!");
            
            rd = req.getRequestDispatcher("funcionario/inserir_produto.jsp");  
        }
        else if(opcao.equals("Alterar")){
            if(this.alterar(req, resp))
                req.setAttribute("msg", "Produto alterado com sucesso!");
            else
                req.setAttribute("msg", "Erro ao alterar produto!");
            
            rd = req.getRequestDispatcher("funcionario/alterar_produto.jsp");  
        }
        
        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(LogicaIngrediente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogicaIngrediente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cadastrar(HttpServletRequest req, HttpServletResponse resp){  

        itemProdutoDao = new ItemProdutoDAO(conexao.conectar());
        
        Produto p = new Produto(0, req.getParameter("descricao"));
        p.setPreco(new BigDecimal(req.getParameter("preco")));
        p.setUnidade(req.getParameter("unidade"));
        p.setCategoria(req.getParameter("categoria"));
        
        int codproduto = produtoDao.inserir(p);

        String[] ingredientes = req.getParameterValues("chk_ingrediente");
        
        if(ingredientes != null){
            for(String codingrediente : ingredientes)
            {
                ItemProduto ip = new ItemProduto();
                ip.setProduto(new Produto(codproduto, ""));
                ip.setIngrediente(new Ingrediente(Integer.parseInt(codingrediente), ""));
                
                String qtde = req.getParameter("qtd_" + codingrediente);
                
                if(qtde.equals(""))
                    ip.setQtde(0);
                else
                    ip.setQtde(Double.parseDouble(qtde));
                
                itemProdutoDao.inserir(ip);
            }        
        }
    }
    
    public boolean alterar(HttpServletRequest req, HttpServletResponse resp){
        /*Produto p = new Produto(Integer.parseInt(req.getParameter("codproduto")), req.getParameter("descricao"));
        p.setPreco(new BigDecimal(req.getParameter("preco")));
        p.setUnidade(req.getParameter("unidade"));
        p.setCategoria(req.getParameter("categoria"));
        
        return produtoDao.alterar(p);*/
        return false;
    }
}
