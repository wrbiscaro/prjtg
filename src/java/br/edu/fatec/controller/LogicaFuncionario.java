/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fatec.controller;

import br.edu.fatec.model.Funcionario;
import br.edu.fatec.model.Loja;
import br.edu.fatec.model.Perfil;
import br.edu.fatec.model.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Wallace Biscaro
 */
public class LogicaFuncionario extends ClasseAbstrata {
    Conexao conexao = null;
    UsuarioDAO usuarioDao = null;
    FuncionarioDAO funcionarioDao = null;
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        conexao = new Conexao();
        RequestDispatcher rd = null;
        String opcao = req.getParameter("Funcionario");

        if(opcao.equals("Cadastrar")){
            if(this.cadastrar(req, resp))
                req.setAttribute("msg", "Usuário cadastrado com sucesso!");
            else
                req.setAttribute("msg", "Erro ao cadastrar usuário!");
            
            rd = req.getRequestDispatcher("gerente/cadastrar_usuario.jsp");     
        }
        else if(opcao.equals("Alterar")){
            this.alterar(req, resp);
            req.setAttribute("msg", "Dados alterados com sucesso!");
            rd = req.getRequestDispatcher("funcionario/editar_dados_funcionario.jsp");     
        }
                
        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(LogicaCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogicaCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    public void alterar(HttpServletRequest req, HttpServletResponse resp){
        usuarioDao = new UsuarioDAO(conexao.conectar());
        funcionarioDao = new FuncionarioDAO(conexao.conectar());
        
        Funcionario f = new Funcionario(Integer.parseInt(req.getParameter("codfuncionario")), req.getParameter("nome"));
        f.setTelefone(req.getParameter("telefone"));
        f.setCpf(req.getParameter("cpf"));
        
        String dtNasc = req.getParameter("dtnasc");
        String arrayData[] = dtNasc.split("/");
        dtNasc = arrayData[2] + "-" + arrayData[1] + "-" + arrayData[0];
        f.setDataNasc(dtNasc);

        funcionarioDao.alterar(f);
        
        int codusuario = Integer.parseInt(req.getParameter("codusuario"));
        
        Usuario u = usuarioDao.selecionar(codusuario);
        u.setEmail(req.getParameter("email"));
        
        if(!req.getParameter("novasenha").equals(""))
            u.setSenha(req.getParameter("novasenha"));
        
        usuarioDao.alterar(u);  
        
        //Verifica se o funcionario alterado e o mesmo que esta na sessao. Se for, grava a sessao novamente
        Funcionario funcionarioSessao = (Funcionario)req.getSession().getAttribute("funcionario");
      
        if(funcionarioSessao != null && funcionarioSessao.getCod() == Integer.parseInt(req.getParameter("codfuncionario")))
        {
            f = funcionarioDao.selecionarPorUsuario(codusuario);

            req.getSession().setAttribute("funcionario", f);
            req.getSession().setAttribute("usuario", u);        
        }
    }
    
    public boolean cadastrar(HttpServletRequest req, HttpServletResponse resp){
        usuarioDao = new UsuarioDAO(conexao.conectar());
        funcionarioDao = new FuncionarioDAO(conexao.conectar());
        
        //Pega o gerente que esta logado no sistema. O gerente so pode cadastrar funcionarios para a sua unidade
        Funcionario funcionarioLogado = (Funcionario)req.getSession().getAttribute("funcionario");
        Loja loja = funcionarioLogado.getLoja();
        
        Usuario u = new Usuario(0, req.getParameter("email"), req.getParameter("senha"));
        u.setPerfil(new Perfil(Integer.parseInt(req.getParameter("perfil")), ""));
        u.setTipoUsuario("F");
        
        int codusuario = usuarioDao.inserir(u);
        u.setCod(codusuario);
        
        Funcionario f = new Funcionario(0, req.getParameter("nome"));
        f.setTelefone(req.getParameter("telefone"));
        f.setCpf(req.getParameter("cpf"));
        
        String dtNasc = req.getParameter("dtnasc");
        String arrayData[] = dtNasc.split("/");
        dtNasc = arrayData[2] + "-" + arrayData[1] + "-" + arrayData[0];
        f.setDataNasc(dtNasc);
        
        f.setUsuario(u);
        f.setLoja(loja);
        
        return (funcionarioDao.inserir(f) != 0);
    }
}
