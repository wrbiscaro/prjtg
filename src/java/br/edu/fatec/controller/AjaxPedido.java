/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class AjaxPedido extends ClasseAbstrata {

    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        Conexao conexao = new Conexao();
        PedidoDAO pedidoDao = new PedidoDAO(conexao.conectar());
        HttpSession sessao = req.getSession();
        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("atualizarStatus")){
                int status = Integer.parseInt(req.getParameter("status"));
                int codpedido = Integer.parseInt(req.getParameter("codpedido"));
                
                boolean update = pedidoDao.atualizarStatus(status, codpedido);
                
                if(update)
                    out.println("<h5><b>Status do pedido #" + codpedido + " atualizado com sucesso!</b></h5>");
                else
                    out.println("<h5><b>Não foi possível atualizar o status do pedido!</b></h5>");  
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }
 
    }
    
}
