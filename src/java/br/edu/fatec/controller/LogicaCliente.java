/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Cliente;
import br.edu.fatec.model.Endereco;
import br.edu.fatec.model.Perfil;
import br.edu.fatec.model.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luizdagoberto
 */
public class LogicaCliente extends ClasseAbstrata {
    Conexao conexao = null;
    UsuarioDAO usuarioDao = null;
    Usuario usuario = null;
    Cliente cliente = null;
    ClienteDAO clienteDao = null;
    Endereco endereco = null;
    EnderecoDAO enderecoDao = null;
    
    @Override
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        conexao = new Conexao();
        RequestDispatcher rd = null;
        String opcao = req.getParameter("Cliente");
        
        if(opcao.equals("CadastrarCompleto")){
            this.cadastrarCompleto(req, resp);
            req.setAttribute("msg", "Cadastrado efetuado com sucesso!");
            rd = req.getRequestDispatcher("index.jsp");     
        }
        else if(opcao.equals("Alterar")){
            this.alterar(req, resp);
            req.setAttribute("msg", "Dados alterados com sucesso!");
            rd = req.getRequestDispatcher("cliente/editar_dados_cliente.jsp");     
        }
                
        try {
            rd.forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(LogicaCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogicaCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cadastrarCompleto(HttpServletRequest req, HttpServletResponse resp){
        usuarioDao = new UsuarioDAO(conexao.conectar());
        clienteDao = new ClienteDAO(conexao.conectar());
        enderecoDao = new EnderecoDAO(conexao.conectar());
        
        HttpSession session = req.getSession();
        
        Usuario usuario = new Usuario(0, req.getParameter("email"), req.getParameter("senha"));
        usuario.setTipoUsuario("C");
        usuario.setPerfil(new Perfil(3, "Cliente"));
        int codusuario = usuarioDao.inserir(usuario);
        usuario.setCod(codusuario);

        Cliente cliente = new Cliente(0, req.getParameter("nome"));
        cliente.setTelefone(req.getParameter("telefone"));
        cliente.setUsuario(usuario);        
        //Trata a data
        String dtNasc = req.getParameter("dtnasc");
        String arrayData[] = dtNasc.split("/");
        dtNasc = arrayData[2] + "-" + arrayData[1] + "-" + arrayData[0];
        cliente.setDataNasc(dtNasc);
        int codcliente = clienteDao.inserir(cliente);
        cliente.setCod(codcliente);
        
        Endereco endereco = (Endereco)session.getAttribute("endereco");
        endereco.setNumero(Integer.parseInt(req.getParameter("numero")));
        endereco.setComplemento(req.getParameter("complemento"));
        endereco.setCliente(cliente);
        int codendereco = enderecoDao.inserir(endereco);
        
        session.setAttribute("cliente", cliente);
        session.setAttribute("usuario", usuario);
        session.removeAttribute("endereco");
    }
    
    public void alterar(HttpServletRequest req, HttpServletResponse resp){
        usuarioDao = new UsuarioDAO(conexao.conectar());
        clienteDao = new ClienteDAO(conexao.conectar());
        
        Cliente c = new Cliente(Integer.parseInt(req.getParameter("codcliente")), req.getParameter("nome"));
        c.setTelefone(req.getParameter("telefone"));
        
        String dtNasc = req.getParameter("dtnasc");
        String arrayData[] = dtNasc.split("/");
        dtNasc = arrayData[2] + "-" + arrayData[1] + "-" + arrayData[0];
        c.setDataNasc(dtNasc);

        clienteDao.alterar(c);
        
        int codusuario = Integer.parseInt(req.getParameter("codusuario"));
        
        Usuario u = usuarioDao.selecionar(codusuario);
        u.setEmail(req.getParameter("email"));
        
        if(!req.getParameter("novasenha").equals(""))
            u.setSenha(req.getParameter("novasenha"));
        
        usuarioDao.alterar(u);       
        
        //Verifica se o cliente alterado e o mesmo que esta na sessao. Se for, grava a sessao novamente
        Cliente clienteSessao = (Cliente)req.getSession().getAttribute("cliente");
      
        if(clienteSessao != null && clienteSessao.getCod() == Integer.parseInt(req.getParameter("codcliente")))
        {
            req.getSession().setAttribute("cliente", c);
            req.getSession().setAttribute("usuario", u);       
        }
    }
}
