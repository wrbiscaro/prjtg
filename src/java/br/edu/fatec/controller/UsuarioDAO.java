/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Funcionario;
import br.edu.fatec.model.Perfil;
import br.edu.fatec.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class UsuarioDAO {
    private Connection conn;

    public UsuarioDAO(Connection conn) {
        this.conn = conn;
    }
    
    public Usuario logar(String email, String senha){
        Usuario u = null;
        Perfil p = null;        
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbusuario u INNER JOIN tbperfil p ON u.cod_perfil = p.cod_perfil WHERE u.email_usuario = ? AND u.senha_usuario = ?");
            ps.setString(1, email);
            ps.setString(2, senha);
            
            ResultSet rs = ps.executeQuery();

            if(rs.first()){
                p = new Perfil(rs.getInt("cod_perfil"), rs.getString("descricao_perfil"));
                
                u = new Usuario(rs.getInt("cod_usuario"), rs.getString("email_usuario"), rs.getString("senha_usuario"));
                u.setTipoUsuario(rs.getString("tipo_usuario"));
                u.setPerfil(p);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return u;
    }
    
    public int inserir(Usuario u){
        int codusuario = 0;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbusuario (email_usuario, senha_usuario, tipo_usuario, cod_perfil) VALUES(?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, u.getEmail());
            ps.setString(2, u.getSenha());
            ps.setString(3, u.getTipoUsuario());
            ps.setInt(4, u.getPerfil().getCod());
            
            ps.execute();

            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next())
                codusuario = rs.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return codusuario;  
    }
    
    public ArrayList<Funcionario> listarPorLoja(int codloja){
        ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("(SELECT c.nome_cliente AS nome, u.*\n" +
                                        "FROM tbusuario u\n" +
                                        "INNER JOIN tbcliente c ON u.cod_usuario = c.cod_usuario) \n" +
                                        "UNION\n" +
                                        "(SELECT f.nome_funcionario AS nome, u.*\n" +
                                        "FROM tbusuario u\n" +
                                        "INNER JOIN tbfuncionario f ON u.cod_usuario = f.cod_usuario WHERE f.cod_loja = ?)\n" +
                                        "ORDER BY nome");
            ps.setInt(1, codloja);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Usuario u = new Usuario(rs.getInt("cod_usuario"), rs.getString("email_usuario"), rs.getString("senha_usuario"));
                u.setTipoUsuario(rs.getString("tipo_usuario"));
                Funcionario f = new Funcionario(0, rs.getString("nome"));
                f.setUsuario(u);
            
                funcionarios.add(f);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return funcionarios;  
    }
    
    public Usuario selecionar(int codusuario){
        Usuario u = null;
        Perfil p = null;        
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbusuario u INNER JOIN tbperfil p ON u.cod_perfil = p.cod_perfil WHERE u.cod_usuario = ?");
            ps.setInt(1, codusuario);
            
            ResultSet rs = ps.executeQuery();

            if(rs.first()){
                p = new Perfil(rs.getInt("cod_perfil"), rs.getString("descricao_perfil"));
                
                u = new Usuario(rs.getInt("cod_usuario"), rs.getString("email_usuario"), rs.getString("senha_usuario"));
                u.setTipoUsuario(rs.getString("tipo_usuario"));
                u.setPerfil(p);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return u;
    }
  
    public void alterar(Usuario u){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbusuario SET email_usuario = ?, senha_usuario = ?, tipo_usuario = ?, cod_perfil= ? WHERE cod_usuario = ?");
            ps.setString(1, u.getEmail());
            ps.setString(2, u.getSenha());
            ps.setString(3, u.getTipoUsuario());
            ps.setInt(4, u.getPerfil().getCod());
            ps.setInt(5, u.getCod());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }
    
    public void excluir(int codusuario){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbusuario WHERE cod_usuario = ?");
            ps.setInt(1, codusuario);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
    
    public void excluirPorLoja(int codloja){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbusuario WHERE cod_usuario IN (SELECT u.cod_usuario FROM tbusuario u INNER JOIN tbfuncionario f ON u.cod_usuario = f.cod_usuario WHERE f.cod_loja = ?)");
            ps.setInt(1, codloja);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
}
