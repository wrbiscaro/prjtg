/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Cliente;
import br.edu.fatec.model.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class EnderecoDAO {
    private Connection conn;

    public EnderecoDAO(Connection conn) {
        this.conn = conn;
    }
    
    public ArrayList<Endereco> selecionarPorUsuario(int codusuario){
        ArrayList<Endereco> enderecos = new ArrayList<Endereco>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT e.*, c.*\n" +
                                       "FROM tbendereco e\n" +
                                       "INNER JOIN tbcliente c ON e.cod_cliente = c.cod_cliente\n" +
                                       "INNER JOIN tbusuario u ON c.cod_usuario = u.cod_usuario\n" +
                                       "WHERE u.cod_usuario = ?");
            ps.setInt(1, codusuario);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Cliente c = new Cliente(rs.getInt("cod_cliente"), rs.getString("nome_cliente"));
                
                Endereco e = new Endereco(rs.getInt("cod_endereco"), rs.getString("cep"));
                e.setEndereco(rs.getString("endereco"));
                e.setNumero(rs.getInt("numero"));
                e.setBairro(rs.getString("bairro"));
                e.setCidade(rs.getString("cidade"));
                e.setUf(rs.getString("uf"));
                e.setComplemento(rs.getString("complemento"));
                e.setCliente(c);
                
                enderecos.add(e);
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return enderecos;
    }  
    
    public Endereco pesquisarPorCEP(String cep){
        PreparedStatement ps = null;
        Endereco endereco = null;
        
        try {
            ps = conn.prepareStatement("SELECT e.nome AS endereco, b.nome AS bairro, c.nome AS cidade, est.sigla AS estado\n" +
                                        "FROM tbcep_endereco e\n" +
                                        "INNER JOIN tbcep_cidade c ON e.codcep_cidade = c.codcep_cidade\n" +
                                        "INNER JOIN tbcep_estado est ON c.codestado = est.codestado\n" +
                                        "INNER JOIN tbcep_bairro b ON e.codcep_bairro_inicio = b.codcep_bairro\n" +                    
                                        "WHERE cep = ?");
            ps.setString(1, cep);

            ResultSet rs = ps.executeQuery();

            if(rs.first()){
                endereco = new Endereco(0, cep);
                endereco.setEndereco(rs.getString("endereco"));
                endereco.setBairro(rs.getString("bairro"));
                endereco.setCidade(rs.getString("cidade"));
                endereco.setUf(rs.getString("estado"));
              
                //endereco = rs.getString("endereco") + ", " + rs.getString("cidade") + " - " + rs.getString("estado");
            }
            else{
                endereco = null;
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }  
        return endereco;
    }
    
    public int inserir(Endereco e){
        int codendereco = 0;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbendereco (cep, endereco, numero, bairro, cidade, uf, complemento, cod_cliente) VALUES(?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, e.getCep());
            ps.setString(2, e.getEndereco());
            ps.setInt(3, e.getNumero());
            ps.setString(4, e.getBairro());
            ps.setString(5, e.getCidade());
            ps.setString(6, e.getUf());
            ps.setString(7, e.getComplemento());
            ps.setInt(8, e.getCliente().getCod());
            
            ps.execute();

            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next())
                codendereco = rs.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return codendereco;  
    }
    
    public Endereco selecionar(int codendereco){
        Endereco e = null;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT * FROM tbendereco WHERE cod_endereco = ?");
            ps.setInt(1, codendereco);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){                
                e = new Endereco(rs.getInt("cod_endereco"), rs.getString("cep"));
                e.setEndereco(rs.getString("endereco"));
                e.setNumero(rs.getInt("numero"));
                e.setBairro(rs.getString("bairro"));
                e.setCidade(rs.getString("cidade"));
                e.setUf(rs.getString("uf"));
                e.setComplemento(rs.getString("complemento"));
            }     
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        
        return e;
    }  
    
    public boolean alterar(Endereco e){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbendereco SET cep = ?, endereco = ?, numero = ?, bairro = ?, cidade = ?, uf = ?, complemento = ? WHERE cod_endereco = ?");
            ps.setString(1, e.getCep());
            ps.setString(2, e.getEndereco());
            ps.setInt(3, e.getNumero());
            ps.setString(4, e.getBairro());
            ps.setString(5, e.getCidade());
            ps.setString(6, e.getUf());
            ps.setString(7, e.getComplemento());
            ps.setInt(8, e.getCod());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }        
        
        return true;  
    }
    
    public void excluirPorCliente(int codcliente){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbendereco WHERE cod_cliente = ?");
            ps.setInt(1, codcliente);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }    
    }
}
