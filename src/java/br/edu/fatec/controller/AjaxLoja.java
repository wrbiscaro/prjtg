/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fatec.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wallace Biscaro
 */
public class AjaxLoja extends ClasseAbstrata{
    UsuarioDAO usuarioDao = null;
    FuncionarioDAO funcionarioDao = null;
    EstoqueDAO estoqueDao = null;
    
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        Conexao conexao = new Conexao();
        LojaDAO lojaDao = new LojaDAO(conexao.conectar());
        HttpSession sessao = req.getSession();
        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("excluir")){
                usuarioDao = new UsuarioDAO(conexao.conectar());
                funcionarioDao = new FuncionarioDAO(conexao.conectar());
                estoqueDao = new EstoqueDAO(conexao.conectar());
                
                int codloja = Integer.parseInt(req.getParameter("codloja"));
                
                usuarioDao.excluirPorLoja(codloja);
                funcionarioDao.excluirPorLoja(codloja);
                estoqueDao.excluirPorLoja(codloja);
                lojaDao.excluir(codloja);
                
                out.println("Loja excluida com sucesso!");  
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }
    }    
}
