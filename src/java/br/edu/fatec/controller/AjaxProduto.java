/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fatec.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wallace Biscaro
 */
public class AjaxProduto extends ClasseAbstrata{
    ProdutoDAO produtoDao = null;
    ItemProdutoDAO itemProdutoDao = null;
    
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        Conexao conexao = new Conexao();
        ProdutoDAO produtoDao = new ProdutoDAO(conexao.conectar());
        ItemProdutoDAO itemProdutoDao = new ItemProdutoDAO(conexao.conectar());
        HttpSession sessao = req.getSession();
        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("excluir")){
                int codproduto = Integer.parseInt(req.getParameter("codproduto"));
                
                itemProdutoDao.excluirPorProduto(codproduto);
                produtoDao.excluir(codproduto);
                
                out.println("Produto excluido com sucesso!");  
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }
    }     
}
