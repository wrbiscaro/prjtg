/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fatec.controller;

import br.edu.fatec.model.Cliente;
import br.edu.fatec.model.Funcionario;
import br.edu.fatec.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wallace Biscaro
 */
public class AjaxUsuario extends ClasseAbstrata{
    UsuarioDAO usuarioDao = null;
    ClienteDAO clienteDao = null;
    FuncionarioDAO funcionarioDao = null;
    EnderecoDAO enderecoDao = null;
    
    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        Conexao conexao = new Conexao();
        IngredienteDAO ingredienteDao = new IngredienteDAO(conexao.conectar());
        HttpSession sessao = req.getSession();
        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("excluir")){
                usuarioDao = new UsuarioDAO(conexao.conectar());
                clienteDao = new ClienteDAO(conexao.conectar());
                funcionarioDao = new FuncionarioDAO(conexao.conectar());
                enderecoDao = new EnderecoDAO(conexao.conectar());
                
                int codusuario = Integer.parseInt(req.getParameter("codusuario"));
                
                Usuario u = usuarioDao.selecionar(codusuario);

                if(u.getTipoUsuario().equals("F")){
                    Funcionario f = funcionarioDao.selecionarPorUsuario(codusuario);
                    
                    usuarioDao.excluir(codusuario);
                    funcionarioDao.excluir(f.getCod());
                }
                else{
                    Cliente c = clienteDao.selecionarPorUsuario(codusuario);
                    
                    usuarioDao.excluir(codusuario);
                    enderecoDao.excluirPorCliente(c.getCod());
                    clienteDao.excluir(c.getCod());
                }
                
                out.println("Usuário excluido com sucesso!");  
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }
    }     
}
