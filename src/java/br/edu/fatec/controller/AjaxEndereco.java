/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fatec.controller;

import br.edu.fatec.model.Endereco;
import br.edu.fatec.model.Loja;
import br.edu.fatec.model.Rota;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Wallace Biscaro
 */
public class AjaxEndereco extends ClasseAbstrata {

    public void executa(HttpServletRequest req, HttpServletResponse resp) {
        Conexao conexao = new Conexao();
        EnderecoDAO enderecoDao = new EnderecoDAO(conexao.conectar());
        LojaDAO lojaDao = new LojaDAO(conexao.conectar());
        Rota r = new Rota();
        HttpSession sessao = req.getSession();
        String acao = req.getParameter("acao");
        
        try {
            PrintWriter out = resp.getWriter();
            //resp.setContentType("text/plain");
            //resp.setContentType("application/json");
            resp.setContentType("text/html");
            req.setCharacterEncoding("UTF-8");  
            resp.setCharacterEncoding("UTF-8");
            
            if(acao.equals("consultar")){
                ArrayList<Loja> unidades = lojaDao.listar();
                
                Endereco endereco = null;
                String strEndereco = null;

                //Pega o cep da requisicao do ajax
                String cep = req.getParameter("cep");
                System.out.println(cep);

                endereco = enderecoDao.pesquisarPorCEP(cep);
                
                if(endereco == null)
                    strEndereco = "Invalido";
                else{
                    double distancia, menorDistancia = 999;
                    strEndereco = endereco.getEndereco() + ", " + endereco.getCidade() + " - " + endereco.getUf();

                    strEndereco = r.removerAcentos(strEndereco);
                    System.out.println(strEndereco);

                    for(Loja u : unidades){
                        String loja = u.getEndereco() + ", " + u.getCidade() + " - " + u.getUf();
                        loja = r.removerAcentos(loja);

                        distancia = r.calcular(loja, strEndereco);

                        if(distancia == -1)
                            System.out.println("Não foi possivel calcular a rota para a unidade " + u);
                        else
                        {
                            System.out.println("Distancia para a unidade " + loja + ": " + distancia);

                            if(distancia < menorDistancia)
                                menorDistancia = distancia;
                        }
                    }
                    System.out.println("Menor distancia para entrega: " + menorDistancia);

                    if(menorDistancia == 999)//Nao foi possivel calcular rota para nenhuma das unidades
                        strEndereco = "Invalido";
                    else
                        strEndereco = endereco.getCep() + ";" + endereco.getEndereco() + ";" + endereco.getBairro() + ";" + endereco.getCidade() + ";" + endereco.getUf();               
                }                
                out.println(strEndereco);
                //response.getWriter().write(dados); 
            }
        } catch (IOException ex) {
            Logger.getLogger(ex.toString());
        }     
    }   
}
