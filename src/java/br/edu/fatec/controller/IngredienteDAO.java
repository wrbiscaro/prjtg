/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.edu.fatec.controller;

import br.edu.fatec.model.Familia;
import br.edu.fatec.model.Ingrediente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author luizdagoberto
 */
public class IngredienteDAO {
    private Connection conn;

    public IngredienteDAO(Connection conn) {
        this.conn = conn;
    }  
    
    public int inserir(Ingrediente i){
        int codingrediente = 0;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("INSERT INTO tbingrediente (descricao_ingrediente, unidade_ingrediente, cod_familia, qtde_padrao, estoque) VALUES(?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, i.getDescricao());
            ps.setString(2, i.getUnidade());
            ps.setInt(3, i.getFamilia().getCod());
            ps.setDouble(4, i.getQtde_padrao());
            ps.setString(5, i.getEstoque());
            
            ps.execute();
            
            ResultSet rs = ps.getGeneratedKeys();
            
            if(rs.next())
                codingrediente = rs.getInt(1);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return codingrediente;  
    }    
    
    public ArrayList<Ingrediente> listar(){
        ArrayList<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT i.*, f.descricao_familia FROM tbingrediente i INNER JOIN tbfamilia f ON i.cod_familia = f.cod_familia ORDER BY descricao_ingrediente");
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                Ingrediente i = new Ingrediente(rs.getInt("cod_ingrediente"), rs.getString("descricao_ingrediente"));
                i.setUnidade(rs.getString("unidade_ingrediente"));
                i.setFamilia(new Familia(rs.getInt("cod_familia"), rs.getString("descricao_familia")));
                i.setQtde_padrao(rs.getDouble("qtde_padrao"));
                i.setEstoque(rs.getString("estoque"));
            
                ingredientes.add(i);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return ingredientes;  
    }
    
    public boolean excluir(int codingrediente){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("DELETE FROM tbingrediente WHERE cod_ingrediente = ?");
            ps.setInt(1, codingrediente);
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }        
        
        return true;  
    }  
    
    public Ingrediente selecionar(int codingrediente){
        Ingrediente i = null;
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("SELECT i.*, f.descricao_familia FROM tbingrediente i INNER JOIN tbfamilia f ON i.cod_familia = f.cod_familia WHERE i.cod_ingrediente = ?");
            ps.setInt(1, codingrediente);
            
            ResultSet rs = ps.executeQuery();
            
            while(rs.next()){
                i = new Ingrediente(rs.getInt("cod_ingrediente"), rs.getString("descricao_ingrediente"));
                i.setUnidade(rs.getString("unidade_ingrediente"));
                i.setFamilia(new Familia(rs.getInt("cod_familia"), rs.getString("descricao_familia")));
                i.setQtde_padrao(rs.getDouble("qtde_padrao"));
                i.setEstoque(rs.getString("estoque"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }        
        
        return i;  
    }
    
    public boolean alterar(Ingrediente i){
        PreparedStatement ps = null;
        
        try {
            ps = conn.prepareStatement("UPDATE tbingrediente SET descricao_ingrediente = ?, unidade_ingrediente = ?, cod_familia = ?, qtde_padrao = ?, estoque = ? WHERE cod_ingrediente = ?");
            ps.setString(1, i.getDescricao());
            ps.setString(2, i.getUnidade());
            ps.setInt(3, i.getFamilia().getCod());
            ps.setDouble(4, i.getQtde_padrao());
            ps.setString(5, i.getEstoque());
            ps.setInt(6, i.getCod());
            
            ps.execute();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return false;
        }        
        
        return true;  
    } 
}
